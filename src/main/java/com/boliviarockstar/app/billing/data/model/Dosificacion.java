package com.boliviarockstar.app.billing.data.model;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by jose on 04/05/2016.
 */
@Entity
public class Dosificacion {
    private Long id;
    private Timestamp fechaInicio;
    private Timestamp fechaFin;
    private Long numeroFacturaInicio;
    private Long numeroFacturaActual;
    private String actividadEconomica;
    private Long numeroAutorizacion;
    private String llave;
    private Long estado;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "fecha_inicio", nullable = true, insertable = true, updatable = true)
    public Timestamp getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Timestamp fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    @Basic
    @Column(name = "fecha_fin", nullable = true, insertable = true, updatable = true)
    public Timestamp getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Timestamp fechaFin) {
        this.fechaFin = fechaFin;
    }

    @Basic
    @Column(name = "numero_factura_inicio", nullable = true, insertable = true, updatable = true)
    public Long getNumeroFacturaInicio() {
        return numeroFacturaInicio;
    }

    public void setNumeroFacturaInicio(Long numeroFacturaInicio) {
        this.numeroFacturaInicio = numeroFacturaInicio;
    }

    @Basic
    @Column(name = "numero_factura_actual", nullable = true, insertable = true, updatable = true)
    public Long getNumeroFacturaActual() {
        return numeroFacturaActual;
    }

    public void setNumeroFacturaActual(Long numeroFacturaActual) {
        this.numeroFacturaActual = numeroFacturaActual;
    }

    @Basic
    @Column(name = "actividad_economica", nullable = true, insertable = true, updatable = true, length = 2048)
    public String getActividadEconomica() {
        return actividadEconomica;
    }

    public void setActividadEconomica(String actividadEconomica) {
        this.actividadEconomica = actividadEconomica;
    }

    @Basic
    @Column(name = "numero_autorizacion", nullable = true, insertable = true, updatable = true)
    public Long getNumeroAutorizacion() {
        return numeroAutorizacion;
    }

    public void setNumeroAutorizacion(Long numeroAutorizacion) {
        this.numeroAutorizacion = numeroAutorizacion;
    }

    @Basic
    @Column(name = "llave", nullable = true, insertable = true, updatable = true, length = 2048)
    public String getLlave() {
        return llave;
    }

    public void setLlave(String llave) {
        this.llave = llave;
    }

    @Basic
    @Column(name = "estado", nullable = true, insertable = true, updatable = true)
    public Long getEstado() {
        return estado;
    }

    public void setEstado(Long estado) {
        this.estado = estado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Dosificacion that = (Dosificacion) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (fechaInicio != null ? !fechaInicio.equals(that.fechaInicio) : that.fechaInicio != null) return false;
        if (fechaFin != null ? !fechaFin.equals(that.fechaFin) : that.fechaFin != null) return false;
        if (numeroFacturaInicio != null ? !numeroFacturaInicio.equals(that.numeroFacturaInicio) : that.numeroFacturaInicio != null)
            return false;
        if (numeroFacturaActual != null ? !numeroFacturaActual.equals(that.numeroFacturaActual) : that.numeroFacturaActual != null)
            return false;
        if (actividadEconomica != null ? !actividadEconomica.equals(that.actividadEconomica) : that.actividadEconomica != null)
            return false;
        if (numeroAutorizacion != null ? !numeroAutorizacion.equals(that.numeroAutorizacion) : that.numeroAutorizacion != null)
            return false;
        if (llave != null ? !llave.equals(that.llave) : that.llave != null) return false;
        if (estado != null ? !estado.equals(that.estado) : that.estado != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (fechaInicio != null ? fechaInicio.hashCode() : 0);
        result = 31 * result + (fechaFin != null ? fechaFin.hashCode() : 0);
        result = 31 * result + (numeroFacturaInicio != null ? numeroFacturaInicio.hashCode() : 0);
        result = 31 * result + (numeroFacturaActual != null ? numeroFacturaActual.hashCode() : 0);
        result = 31 * result + (actividadEconomica != null ? actividadEconomica.hashCode() : 0);
        result = 31 * result + (numeroAutorizacion != null ? numeroAutorizacion.hashCode() : 0);
        result = 31 * result + (llave != null ? llave.hashCode() : 0);
        result = 31 * result + (estado != null ? estado.hashCode() : 0);
        return result;
    }
}
