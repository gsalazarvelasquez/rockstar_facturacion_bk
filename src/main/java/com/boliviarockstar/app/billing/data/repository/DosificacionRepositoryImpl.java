package com.boliviarockstar.app.billing.data.repository;

import com.boliviarockstar.app.billing.data.model.Dosificacion;
import com.boliviarockstar.shared.persistence.Repository;

import javax.ejb.Stateless;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by daniel on 11/02/2016.
 */
@Stateless
public class DosificacionRepositoryImpl extends Repository {

    @Override
    protected Class getPersistentClass() {
        return this.getClass();
    }

    public Dosificacion getEnableDosage() {
        Dosificacion result = null;
        try {
            Query query = em.createQuery("SELECT dosificacion FROM Dosificacion dosificacion " +
                    "WHERE dosificacion.estado = :estado " +
                    "AND (NOW() BETWEEN dosificacion.fechaInicio AND dosificacion.fechaFin) " +
                    "ORDER BY dosificacion.id ASC");
            query.setParameter("estado", 1L);
            List list = query.getResultList();
            if(list != null && !list.isEmpty())
                result = (Dosificacion) list.get(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public Dosificacion getDosage(long id) {
        Dosificacion result = null;
        try {
            Query query = em.createQuery("SELECT dosificacion FROM Dosificacion dosificacion WHERE dosificacion.id = :id");
            query.setParameter("id", id);
            List list = query.getResultList();
            if(list != null && !list.isEmpty())
                result = (Dosificacion) list.get(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public List<Dosificacion> getDosages() {
        List<Dosificacion> result = null;
        try {
            Query query = em.createQuery("SELECT dosificacion FROM Dosificacion dosificacion ORDER BY dosificacion.id ASC");
            result = query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
