package com.boliviarockstar.app.billing.data.repository;

import com.boliviarockstar.app.billing.data.model.FundempresaInfo;
import com.boliviarockstar.shared.persistence.Repository;

import javax.ejb.Stateless;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by daniel on 11/02/2016.
 */
@Stateless
public class FundempresaInfoRepositoryImpl extends Repository {

    @Override
    protected Class getPersistentClass() {
        return this.getClass();
    }

    public FundempresaInfo getInfo() {
        FundempresaInfo result = null;
        try {
            Query query = em.createQuery("SELECT info FROM FundempresaInfo info");
            List list = query.getResultList();
            if(list != null && !list.isEmpty())
                result = (FundempresaInfo) list.get(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
