package com.boliviarockstar.app.billing.data.repository;

import com.boliviarockstar.app.billing.data.model.FundempresaInfo;
import com.boliviarockstar.shared.persistence.Repository;

import java.util.Optional;

/**
 * Created by daniel on 11/02/2016.
 */
public class InfoRepositoryImpl extends Repository {

    public Optional<FundempresaInfo> getInfo() {
        return getById(FundempresaInfo.class, 1L);
    }

    @Override
    protected Class getPersistentClass() {
        return InfoRepositoryImpl.class;
    }
}
