package com.boliviarockstar.app.billing.data.repository;

import com.boliviarockstar.app.billing.data.model.Facturas;
import com.boliviarockstar.shared.persistence.Repository;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by daniel on 11/02/2016.
 */
public class InvoiceRepositoryImpl extends Repository {

    public boolean save(Facturas invoice) {
        saveAndFlush(invoice);
        return true;
    }

    public List<Facturas> getAll() {
        List<Facturas> homonimiaList = this.findAll(Facturas.class);
        Collections.sort(homonimiaList, new Comparator<Facturas>() {
            @Override
            public int compare(Facturas o1, Facturas o2) {
                return o2.getFecha() != null && o1.getFecha() != null
                        ? o2.getFecha().compareTo(o1.getFecha())
                        : o2.getId().compareTo(o1.getId());
            }
        });

        return homonimiaList;
    }

    @Override
    protected Class getPersistentClass() {
        return InvoiceRepositoryImpl.class;
    }
}
