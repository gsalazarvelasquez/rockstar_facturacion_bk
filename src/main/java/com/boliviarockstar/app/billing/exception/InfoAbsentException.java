package com.boliviarockstar.app.billing.exception;

/**
 * Created by daniel on 31/03/2016.
 */
public class InfoAbsentException extends Exception {

    public InfoAbsentException(){
        super("No existe informacion suficiente para procesar su transaccion.");
    }

    public InfoAbsentException(Throwable throwable){
        super(throwable);
    }
}
