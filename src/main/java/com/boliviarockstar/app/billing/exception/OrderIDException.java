package com.boliviarockstar.app.billing.exception;

/**
 * Created by daniel on 31/03/2016.
 */
public class OrderIDException extends Exception {

    public OrderIDException(){
        super("No se pudo procesar su transaccion.");
    }

    public OrderIDException(Throwable throwable){
        super(throwable);
    }
}
