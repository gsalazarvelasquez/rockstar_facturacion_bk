package com.boliviarockstar.app.billing.exception;

/**
 * Created by daniel on 31/03/2016.
 */
public class PasswordException extends Exception {

    public PasswordException(){
        super("Clave incorrecta para procesar el pago.");
    }

    public PasswordException(Throwable throwable){
        super(throwable);
    }
}
