package com.boliviarockstar.app.billing.exception;

/**
 * Created by daniel on 31/03/2016.
 */
public class TMException extends Exception {

    public TMException(){
        super(": No se pudo procesar su transaccion.");
    }

    public TMException(String message){
        super("No se pudo procesar su transaccion: " + message);
    }

    public TMException(Exception e){
        super(e.getMessage());
    }

    public TMException(Throwable throwable){
        super(throwable);
    }
}
