package com.boliviarockstar.app.billing.services;

import com.boliviarockstar.app.billing.data.model.Facturas;

import javax.ejb.Local;
import java.io.OutputStream;
import java.util.List;

/**
 * Created by miguel on 1/26/16.
 */
@Local
public interface InvoiceService {

    List<Facturas> findAll();

    Facturas test();

    // TODO: Implement commented methods

//      Facturas generate(PaymentRequestDto requestDto, String detail) throws DosageException, ParseException, InfoAbsentException;
//
//      Facturas save(PaymentRequestDto requestDto, String detail, int estado) throws InfoAbsentException;

    String printPDF(OutputStream outputStream, Facturas invoice) throws Exception;

}
