package com.boliviarockstar.app.billing.services.internal;

import com.boliviarockstar.app.billing.data.model.Facturas;
import com.boliviarockstar.app.billing.data.repository.DosificacionRepositoryImpl;
import com.boliviarockstar.app.billing.data.repository.InfoRepositoryImpl;
import com.boliviarockstar.app.billing.data.repository.InvoiceRepositoryImpl;
import com.boliviarockstar.app.billing.services.InvoiceService;
import com.boliviarockstar.app.billing.services.internal.invoicegenerator.NumberToLiteral;
import com.boliviarockstar.app.billing.services.internal.invoicegenerator.NumberUtils;
import com.boliviarockstar.app.billing.services.internal.invoicegenerator.QrService;
import com.boliviarockstar.app.billing.services.internal.invoicegenerator.ReportManager;
import com.boliviarockstar.app.impuestos.services.ControlGenerationService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Stateless
public class InvoiceServiceImpl implements InvoiceService {

    private static final String LOGO = "LOGO";
    private static final String CUSTOMER_NAME = "CUSTOMER_NAME";
    private static final String TYPE = "TYPE";
    private static final String QR = "QR";
    private static final String CENTER_NIT = "CENTER_NIT";
    private static final String CUSTOMER_NIT = "CUSTOMER_NIT";
    private static final String INVOICE_NUMBER = "INVOICE_NUMBER";
    private static final String AUTH_NUMBER = "AUTH_NUMBER";
    private static final String CONTROL_CODE = "CONTROL_CODE";
    private static final String LIMIT_DATE = "LIMIT_DATE";
    private static final String INVOICE_DATE = "INVOICE_DATE";
    private static final String AMOUNT_LITERAL = "AMOUNT_LITERAL";
    private static final String AMOUNT_PARTIAL = "AMOUNT_PARTIAL";
    private static final String AMOUNT_TOTAL = "AMOUNT_TOTAL";
    private static final String DISCOUNT = "DISCOUNT";
    private static final String BONUS = "BONUS";
    private static final String CENTER_NAME = "CENTER_NAME";
    private static final String CENTER_ACTIVITY = "CENTER_ACTIVITY";
    private static final String CENTER_ADDRESS = "CENTER_ADDRESS";
    private static final String CENTER_PHONE_CITY = "CENTER_PHONE_CITY";
    private static final String BRANCH_NAME = "BRANCH_NAME";
    private static final String BRANCH_ADDRESS = "BRANCH_ADDRESS";
    private static final String BRANCH_PHONE = "BRANCH_PHONE";
    private static final String BRANCH_CITY = "BRANCH_CITY";
    private static final String SALE_ID = "SALE_ID";
    private static final String SALESMAN_CODE = "SALESMAN_CODE";
    private static final String CUSTOMER_ID = "CUSTOMER_ID";
    private static final String PAYMENT_MODE = "PAYMENT_MODE";

    private static final String DETAILS_QUANTITY = "quantity";
    private static final String DETAILS_UNIT = "unit";
    private static final String DETAILS_PRICE = "price";
    private static final String DETAILS_AMMOUNT = "amount";
    private static final String DETAILS_TOTAL_PRICE = "totalPrice";
    private static final String DETAILS_DISCOUNT = "discount";
    private static final String DETAILS_BONUS = "bonus";
    private static final String DETAILS_PRODUCT_NAME = "productName";
    private static final String DETAILS_PRODUCT_CODE = "productCode";

    @Inject
    ReportManager reportManager;

    @Inject
    InvoiceRepositoryImpl invoiceRepositoryImpl;

    @Inject
    private DosificacionRepositoryImpl dosificacionRepository;

    @Inject
    private InfoRepositoryImpl infoRepositoryImpl;

    @Inject
    private ControlGenerationService codeControlService;

    @Inject
    private QrService qrService;

    @Override
    public List<Facturas> findAll() {
        return this.invoiceRepositoryImpl.getAll();
    }

    @Override
    public Facturas test() {
        return this.invoiceRepositoryImpl.getById(Facturas.class, 2L).get();
    }

//    @Override
//    public Facturas generate(PaymentRequestDto requestDto, String detail) throws DosageException, ParseException, InfoAbsentException {
//        //TODO: GET DOSAGE
//        Dosificacion dosage = dosificacionRepository.getEnableDosage();
//        if(dosage == null){
//            throw new DosageException();
//        }
//        if (!validateDosageLimitDate(dosage.getFechaFin())) {
//            throw new DosageException("La dosificacion ha expirado.");
//        }
//
//        long actual = dosage.getNumeroFacturaActual();
//        dosage.setNumeroFacturaActual(actual + 1);
//        dosificacionRepository.update(dosage);
//
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
//        String dateStr = simpleDateFormat.format(new Date());
//
//        ControlEntityDto control = new ControlEntityDto();
//        control.setAmountStr(requestDto.getMonto());
//        control.setAuthorizationStr(String.valueOf(dosage.getNumeroAutorizacion()));
//        control.setClientNit(requestDto.getNit());
//        control.setDateStr(dateStr);
//        control.setDosageKey(dosage.getLlave());
//        control.setInvoiceNumberStr(String.valueOf(dosage.getNumeroFacturaActual()));
//
//        String controlCode = codeControlService.generate(control);
//        Facturas invoice = createInvoice(dosage, controlCode, requestDto, detail, 1);
//        invoiceRepositoryImpl.save(invoice);
//        return invoice;
//    }
//
//    @Override
//    public Facturas save(PaymentRequestDto requestDto, String detail, int estado) throws InfoAbsentException {
//        Dosificacion dosage = new Dosificacion();
//        dosage.setNumeroFacturaActual(0L);
//        dosage.setNumeroAutorizacion(0L);
//        dosage.setFechaFin(new Timestamp(System.currentTimeMillis()));
//        Facturas invoice = createInvoice(dosage, null, requestDto, detail, estado);
//        invoiceRepositoryImpl.save(invoice);
//        return invoice;
//    }

    @Override
    public String printPDF(OutputStream outputStream, Facturas invoice) throws Exception {
        String jrxml = "invoice.jrxml";
//        String jrxmlFile = Thread.currentThread().getContextClassLoader().getResource(jrxml).getPath();
//        String path = jrxmlFile.replace(".jrxml", ".jasper");
//        System.out.println("***********************" + path);
        String path = jrxml.replace(".jrxml", ".jasper");
//        JasperCompileManager.compileReportToFile(jrxmlFile, path);
        ReportManager.Report report = prepareInvoice(path, invoice);
        reportManager.createReportInPdf(outputStream, report);

        invoice = invoiceRepositoryImpl.getById(Facturas.class, invoice.getId()).get();
        invoice.setEstadoImpresion(1);
        invoiceRepositoryImpl.update(invoice);
        return path;
    }

    // TODO: Fix this code

//    private Facturas createInvoice(Dosificacion dosageEntity, String codeControl, PaymentRequestDto requestDto, String detail, int estado) throws InfoAbsentException {
//        //PREPARING INFO
//        Optional<FundempresaInfo> object = infoRepositoryImpl.getInfo();
//        if(!object.isPresent()) {
//            throw new InfoAbsentException();
//        }
//        FundempresaInfo info = object.get();
//        Facturas factura = new Facturas();
//
//        // SETTING COMMERCE INFO
//        factura.setEmisorNombre(info.getEmisorNombre());
//        factura.setEmisorNit(info.getEmisorNit());
//        factura.setEmisorActividadComercial(info.getEmisorActividadComercial());
//        factura.setEmisorDireccion(info.getEmisorDireccion());
//        factura.setEmisorTelefonos(info.getEmisorTelefonos());
//        factura.setEmisorCiudad(info.getEmisorCiudad());
//
//        // SETTING INVOICE INFO
//        long nroFactura = dosageEntity.getNumeroFacturaActual();
//        nroFactura = nroFactura == 0L ? requestDto.getOrderId() : nroFactura;
//        factura.setNumeroFactura(nroFactura);
//        factura.setNumeroAutorizacion(String.valueOf(dosageEntity.getNumeroAutorizacion()));
//        factura.setFecha(new Timestamp(System.currentTimeMillis()));
//        factura.setFechaLimiteEmision(dosageEntity.getFechaFin());
//        factura.setMontoFactura(requestDto.getMonto());
//        factura.setMontoCreditoFiscal(requestDto.getMonto()); //TODO:ASK IF 100% CREDITO_FISCAL APPLIES
//        factura.setCodigoControl(codeControl);
//
//        //SETTING CUSTOMER INFO
//        factura.setClienteRazonsocial(requestDto.getRazonSocial().replace("+", " "));
//        factura.setClienteNit(requestDto.getNit());
//
//        //SETTING EXTRAS FOR INVOICE
//        factura.setDescuento(BigDecimal.ZERO.toPlainString());
//        factura.setEstadoImpresion(0);//1=ALREADY_PRINTED;0=NOT_PRINTED_YET
//        factura.setEstadoFactura(estado);//1=PROCESSED;0=ANNULLED;2=PENDING
//        factura.setMetodoPago(requestDto.getTipo());//1=TIGO_MONEY;2=PAGOSNET;3=PAYPAL;4=2CHECKOUT
//        factura.setDetalleItem(detail);
//
//        return factura;
//    }

    private boolean validateDosageLimitDate(Timestamp endDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Calendar cal = Calendar.getInstance();
        String today = sdf.format(new Date(cal.getTimeInMillis()));
        cal.setTimeInMillis(endDate.getTime());
        String fechaFin = sdf.format(new Date(cal.getTimeInMillis()));
        return today.compareTo(fechaFin) <= 0;
    }

    private ReportManager.Report prepareInvoice(String path, Facturas invoice) {
        HashMap<String, Object> parameters = new HashMap<String, Object>();

        parameters.put(DETAILS_QUANTITY, "1");
        parameters.put(DETAILS_UNIT, "");
        parameters.put(DETAILS_PRICE, invoice.getMontoFactura());
        parameters.put(DETAILS_AMMOUNT, invoice.getMontoFactura());
        parameters.put(DETAILS_TOTAL_PRICE, invoice.getMontoFactura());
        parameters.put(DETAILS_DISCOUNT, "0");
        parameters.put(DETAILS_BONUS, "0");
        parameters.put(DETAILS_PRODUCT_NAME, invoice.getDetalleItem());
        parameters.put(DETAILS_PRODUCT_CODE, "1");

        parameters.put(LOGO, "logo.jpg");
        InputStream _qr = qrService.getQR(invoice);
        parameters.put(QR, _qr);
        parameters.put(TYPE, (invoice.getEstadoImpresion() == 1) ? "COPIA" : "ORIGINAL");

        parameters.put(CENTER_NIT, invoice.getEmisorNit());
        parameters.put(CUSTOMER_NIT, invoice.getClienteNit());

        parameters.put(CUSTOMER_NAME, invoice.getClienteRazonsocial());
        parameters.put(INVOICE_NUMBER, String.valueOf(invoice.getNumeroFactura()));
        parameters.put(AUTH_NUMBER, invoice.getNumeroAutorizacion());
        parameters.put(CONTROL_CODE, invoice.getCodigoControl());
        parameters.put(LIMIT_DATE, new SimpleDateFormat("dd/MM/yyyy").format(new Date(invoice.getFechaLimiteEmision().getTime())));

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(invoice.getFecha().getTime());
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        String month = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, new Locale("es", "ES"));
        int year = calendar.get(Calendar.YEAR);

        parameters.put(INVOICE_DATE, MessageFormat.format("{0}, {1} de {2} de {3}", invoice.getEmisorCiudad(), day, month, String.valueOf(year)));
        parameters.put(AMOUNT_LITERAL, NumberToLiteral.convert(invoice.getMontoCreditoFiscal(), true));
        parameters.put(AMOUNT_PARTIAL, NumberUtils.format(new BigDecimal(invoice.getMontoFactura()), NumberUtils.FORMAT_DECIMAL_DEFAULT));
        parameters.put(AMOUNT_TOTAL, NumberUtils.format(new BigDecimal(invoice.getMontoCreditoFiscal()), NumberUtils.FORMAT_DECIMAL_DEFAULT));
        parameters.put(DISCOUNT, NumberUtils.format(new BigDecimal(invoice.getDescuento()), NumberUtils.FORMAT_DECIMAL_DEFAULT));
        parameters.put(BONUS, NumberUtils.format(new BigDecimal(invoice.getDescuento()), NumberUtils.FORMAT_DECIMAL_DEFAULT));

        parameters.put(CENTER_NAME, invoice.getEmisorNombre());
        parameters.put(CENTER_ACTIVITY, invoice.getEmisorActividadComercial());
        parameters.put(CENTER_ADDRESS, invoice.getEmisorDireccion());
        parameters.put(CENTER_PHONE_CITY, MessageFormat.format("Telef. {0}", invoice.getEmisorTelefonos()));

        parameters.put(BRANCH_NAME, invoice.getEmisorNombre());
        parameters.put(BRANCH_ADDRESS, invoice.getEmisorDireccion());
        parameters.put(BRANCH_PHONE, MessageFormat.format("Telef. {0}", invoice.getEmisorTelefonos()));
        parameters.put(BRANCH_CITY, invoice.getEmisorCiudad());

        parameters.put(SALE_ID, "");
        parameters.put(SALESMAN_CODE, "");
        parameters.put(CUSTOMER_ID, "");
        parameters.put(PAYMENT_MODE, invoice.getMetodoPago());

        return new ReportManager.Report(path, parameters);
    }
}
