/*-*
 * FILENAME  :
 *    $HeadURL$
 *
 * STATUS  :
 *    $Revision$
 *
 *    $Author$
 *    $Date$
 *  
 *    
 * Copyright (c) 2006 SwissBytes Ltda. All rights reserved.
 *
 ****************************************************************/

package com.boliviarockstar.app.billing.services.internal.invoicegenerator;

/**
 * @author Geremias Gonzalez
 */
public enum DecimalPatternType {
    /**/ONE_DECIMAL(1, "#,##0.0"),
    /**/TWO_DECIMAL(2, "#,##0.00"),
    /**/THREE_DECIMAL(3, "#,##0.000"),
    /**/FOUR_DECIMAL(4, "#,##0.0000"),
    /**/FIVE_DECIMAL(5, "#,##0.00000");

    private final int nroDecimal;

    private final String decimalPattern;

    private DecimalPatternType(final int nroDecimal, final String decimalPattern) {
        this.nroDecimal = nroDecimal;
        this.decimalPattern = decimalPattern;
    }

    public int getNroDecimal() {
        return nroDecimal;
    }

    public String getDecimalPattern() {
        return decimalPattern;
    }

    public static DecimalPatternType of(final int nroDecimal) {
        for (final DecimalPatternType item : values()) {
            if (item.nroDecimal == nroDecimal) {
                return item;
            }
        }
        throw new IllegalArgumentException("documentTypeId [" + nroDecimal + "]");
    }
}
