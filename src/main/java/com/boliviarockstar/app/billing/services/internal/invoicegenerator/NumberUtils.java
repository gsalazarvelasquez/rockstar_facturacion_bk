package com.boliviarockstar.app.billing.services.internal.invoicegenerator;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * @author Geremias Gonzalez
 */
public class NumberUtils {

    private static final Locale locale = Locale.US;
    public static final String FORMAT_DECIMAL_DEFAULT = "#,##0.00";

    public static String format(Number number, final String format) {
        NumberFormat numberFormat = getDecimalFormat(format);
        return numberFormat.format(number != null ? number.doubleValue() : 0D);
    }

    private static NumberFormat getDecimalFormat(String pattern) {
        DecimalFormatSymbols symbols = new DecimalFormatSymbols(locale);
        return new DecimalFormat(pattern, symbols);
    }
}
