package com.boliviarockstar.app.billing.services.internal.invoicegenerator;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;

/**
 * It is able to encode text to qr code
 * 
 * @author Alvaro Cardozo
 */
public class QrScanner {

    private Integer width = 116;
    private Integer height = 116;
    private Integer margin = 2;

    public BufferedImage encodeTextToBuffer(final String text) {
        BitMatrix matrix = getBitMatrix(text);
        return getBufferedImage(matrix);
    }

    private BufferedImage getBufferedImage(final BitMatrix matrix) {
        BufferedImage buffer = null;
        if (matrix != null) {
            buffer = MatrixToImageWriter.toBufferedImage(matrix);
        }
        return buffer;
    }

    private BitMatrix getBitMatrix(final String text) {
        QRCodeWriter writer = new QRCodeWriter();
        BitMatrix matrix = null;
        try {
            if (!text.trim().isEmpty()) {
                Map<EncodeHintType, Object> features = new HashMap<EncodeHintType, Object>();
                features.put(EncodeHintType.MARGIN, margin);
                features.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
                matrix = writer.encode(text, BarcodeFormat.QR_CODE, width, height, features);
            }
        } catch (WriterException e) {
        }
        return matrix;
    }

    public void setSize(final Integer width, final Integer height) {
        this.width = width;
        this.height = height;
    }
}
