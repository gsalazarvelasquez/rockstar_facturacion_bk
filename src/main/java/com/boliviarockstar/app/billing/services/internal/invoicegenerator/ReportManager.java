package com.boliviarockstar.app.billing.services.internal.invoicegenerator;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.base.JRBasePrintPage;
import net.sf.jasperreports.engine.base.JRBasePrintText;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.type.HorizontalAlignEnum;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

public class ReportManager implements Serializable {

    private static final Logger LOG = Logger.getLogger(ReportManager.class.getName());

    private static final int TEXT_Y_AXIS_VALUE = 65;
    private static final int TEXT_X_AXIS_VALUE = 65;
    private static final int TEXT_WIDTH = 445;
    private static final int TEXT_HEIGHT = 70;
    private static final float TEXT_FONT_SIZE = 24f;
    private static final float TEXT_LINE_SPACING_FACTOR_VALUE = 1.329241f;
    private static final float TEXT_LEADING_OFFSET_VALUE = -4.076172f;

    public void createReportInPdf(OutputStream outputStream, Report report) throws Exception {
        try {
            JasperPrint jasperPrint = createReport(report.getPath(), report.getParameters());

            LOG.info("Exporting to PDF...");

            List<JasperPrint> jasperPrintList = new ArrayList<JasperPrint>();
            jasperPrintList.add(jasperPrint);

            exportToPDF(outputStream, jasperPrintList);

        } catch (IOException e) {
            LOG.info("IO Exception on create report " + e.getMessage());
            throw e;
        } catch (SQLException e) {
            LOG.info("SqlException on create report " + e.getMessage());
            throw e;
        } catch (Exception e) {
            LOG.info("Exception on create report " + e.getMessage());
            throw e;
        }
    }

    private JasperPrint createReport(String reportPath, HashMap<String, Object> parameters)
            throws IOException, JRException, SQLException {

        JasperPrint jasperPrint = getJasperReport(reportPath, parameters);
        if (jasperPrint.getPages().isEmpty()) {
            JRPrintPage page = new JRBasePrintPage();
            JRPrintText text = new JRBasePrintText(jasperPrint.getDefaultStyleProvider());
            text.setY(TEXT_Y_AXIS_VALUE);
            text.setX(TEXT_X_AXIS_VALUE);
            text.setWidth(TEXT_WIDTH);
            text.setHeight(TEXT_HEIGHT);
            text.setHorizontalAlignment(HorizontalAlignEnum.CENTER);
            text.setTextHeight(text.getHeight());
            text.setFontSize(TEXT_FONT_SIZE);
            text.setText("No hay datos");
            text.setLineSpacingFactor(TEXT_LINE_SPACING_FACTOR_VALUE);
            text.setLeadingOffset(TEXT_LEADING_OFFSET_VALUE);
            page.addElement(text);
            jasperPrint.addPage(page);
        }
        return jasperPrint;
    }

    private void exportToPDF(OutputStream outputStream, List<JasperPrint> reports) throws Exception {
//    private void exportToPDF(List<JasperPrint> reports) throws Exception {
//        FacesContext facesContext = FacesContext.getCurrentInstance();
//        HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
//        OutputStream outputStream = response.getOutputStream();
//        response.setContentType("application/pdf");
//        facesContext.responseComplete();

        JRPdfExporter exporter = new JRPdfExporter();

        exporter.setExporterInput(SimpleExporterInput.getInstance(reports));
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputStream));
        SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
        exporter.setConfiguration(configuration);

        exporter.exportReport();

        outputStream.flush();
        outputStream.close();
    }

    private JasperPrint getJasperReport(String reportPath, HashMap<String, Object> parameters)
            throws IOException, JRException, SQLException {
        InputStream input = null;
        JasperPrint jasperPrint = null;
        Connection connection = null;
        try {

            input = Thread.currentThread().getContextClassLoader().getResourceAsStream(reportPath);
            if (input == null) {
                throw new JRRuntimeException("File not found: " + reportPath);
            }

            jasperPrint = JasperFillManager.fillReport(input, parameters, new JREmptyDataSource());

        } catch (Exception e) {
            LOG.info("Exception " + e.getMessage());
            jasperPrint = null;
//            throw e;
        } finally {
            if (input != null) {
                input.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
        return jasperPrint;
    }

    public static class Report {
        private String path;
        private HashMap<String, Object> parameters;

        public Report(String path, HashMap<String, Object> parameters) {
            this.path = path;
            this.parameters = parameters;
        }

        public String getPath() {
            return path;
        }

        public HashMap<String, Object> getParameters() {
            return parameters;
        }
    }
}
