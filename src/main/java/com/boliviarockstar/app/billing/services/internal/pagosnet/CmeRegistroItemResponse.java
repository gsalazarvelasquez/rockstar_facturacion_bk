
package com.boliviarockstar.app.billing.services.internal.pagosnet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cmeRegistroItemResult" type="{http://www.openuri.org/}RespItem" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cmeRegistroItemResult"
})
@XmlRootElement(name = "cmeRegistroItemResponse")
public class CmeRegistroItemResponse {

    protected RespItem cmeRegistroItemResult;

    /**
     * Gets the value of the cmeRegistroItemResult property.
     * 
     * @return
     *     possible object is
     *     {@link RespItem }
     *     
     */
    public RespItem getCmeRegistroItemResult() {
        return cmeRegistroItemResult;
    }

    /**
     * Sets the value of the cmeRegistroItemResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link RespItem }
     *     
     */
    public void setCmeRegistroItemResult(RespItem value) {
        this.cmeRegistroItemResult = value;
    }

}
