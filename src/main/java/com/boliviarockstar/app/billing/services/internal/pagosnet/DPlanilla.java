
package com.boliviarockstar.app.billing.services.internal.pagosnet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DPlanilla complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DPlanilla">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="numeroPago" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="montoPago" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="montoCreditoFiscal" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="nombreFactura" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nitFactura" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DPlanilla", propOrder = {
    "numeroPago",
    "montoPago",
    "descripcion",
    "montoCreditoFiscal",
    "nombreFactura",
    "nitFactura"
})
public class DPlanilla {

    protected int numeroPago;
    protected double montoPago;
    protected String descripcion;
    protected double montoCreditoFiscal;
    protected String nombreFactura;
    protected String nitFactura;

    /**
     * Gets the value of the numeroPago property.
     * 
     */
    public int getNumeroPago() {
        return numeroPago;
    }

    /**
     * Sets the value of the numeroPago property.
     * 
     */
    public void setNumeroPago(int value) {
        this.numeroPago = value;
    }

    /**
     * Gets the value of the montoPago property.
     * 
     */
    public double getMontoPago() {
        return montoPago;
    }

    /**
     * Sets the value of the montoPago property.
     * 
     */
    public void setMontoPago(double value) {
        this.montoPago = value;
    }

    /**
     * Gets the value of the descripcion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * Sets the value of the descripcion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescripcion(String value) {
        this.descripcion = value;
    }

    /**
     * Gets the value of the montoCreditoFiscal property.
     * 
     */
    public double getMontoCreditoFiscal() {
        return montoCreditoFiscal;
    }

    /**
     * Sets the value of the montoCreditoFiscal property.
     * 
     */
    public void setMontoCreditoFiscal(double value) {
        this.montoCreditoFiscal = value;
    }

    /**
     * Gets the value of the nombreFactura property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreFactura() {
        return nombreFactura;
    }

    /**
     * Sets the value of the nombreFactura property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreFactura(String value) {
        this.nombreFactura = value;
    }

    /**
     * Gets the value of the nitFactura property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNitFactura() {
        return nitFactura;
    }

    /**
     * Sets the value of the nitFactura property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNitFactura(String value) {
        this.nitFactura = value;
    }

}
