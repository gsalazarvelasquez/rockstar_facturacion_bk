
package com.boliviarockstar.app.billing.services.internal.pagosnet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DatosPlanilla complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DatosPlanilla">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="transaccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nit_CI_cliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoEmpresa" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="fecha" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="hora" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="correoElectronico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="moneda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoRecaudacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descripcionRecaudacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechaVencimiento" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="horaVencimiento" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="codigoProducto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="planillas" type="{http://www.openuri.org/}ArrayOfDPlanilla" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DatosPlanilla", propOrder = {
    "transaccion",
    "nombre",
    "nitCICliente",
    "codigoCliente",
    "codigoEmpresa",
    "fecha",
    "hora",
    "correoElectronico",
    "moneda",
    "codigoRecaudacion",
    "descripcionRecaudacion",
    "fechaVencimiento",
    "horaVencimiento",
    "codigoProducto",
    "planillas"
})
public class DatosPlanilla {

    protected String transaccion;
    protected String nombre;
    @XmlElement(name = "nit_CI_cliente")
    protected String nitCICliente;
    protected String codigoCliente;
    protected int codigoEmpresa;
    protected int fecha;
    protected int hora;
    protected String correoElectronico;
    protected String moneda;
    protected String codigoRecaudacion;
    protected String descripcionRecaudacion;
    protected int fechaVencimiento;
    protected int horaVencimiento;
    protected String codigoProducto;
    protected ArrayOfDPlanilla planillas;

    /**
     * Gets the value of the transaccion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransaccion() {
        return transaccion;
    }

    /**
     * Sets the value of the transaccion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransaccion(String value) {
        this.transaccion = value;
    }

    /**
     * Gets the value of the nombre property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Sets the value of the nombre property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombre(String value) {
        this.nombre = value;
    }

    /**
     * Gets the value of the nitCICliente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNitCICliente() {
        return nitCICliente;
    }

    /**
     * Sets the value of the nitCICliente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNitCICliente(String value) {
        this.nitCICliente = value;
    }

    /**
     * Gets the value of the codigoCliente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoCliente() {
        return codigoCliente;
    }

    /**
     * Sets the value of the codigoCliente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoCliente(String value) {
        this.codigoCliente = value;
    }

    /**
     * Gets the value of the codigoEmpresa property.
     * 
     */
    public int getCodigoEmpresa() {
        return codigoEmpresa;
    }

    /**
     * Sets the value of the codigoEmpresa property.
     * 
     */
    public void setCodigoEmpresa(int value) {
        this.codigoEmpresa = value;
    }

    /**
     * Gets the value of the fecha property.
     * 
     */
    public int getFecha() {
        return fecha;
    }

    /**
     * Sets the value of the fecha property.
     * 
     */
    public void setFecha(int value) {
        this.fecha = value;
    }

    /**
     * Gets the value of the hora property.
     * 
     */
    public int getHora() {
        return hora;
    }

    /**
     * Sets the value of the hora property.
     * 
     */
    public void setHora(int value) {
        this.hora = value;
    }

    /**
     * Gets the value of the correoElectronico property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorreoElectronico() {
        return correoElectronico;
    }

    /**
     * Sets the value of the correoElectronico property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorreoElectronico(String value) {
        this.correoElectronico = value;
    }

    /**
     * Gets the value of the moneda property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMoneda() {
        return moneda;
    }

    /**
     * Sets the value of the moneda property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMoneda(String value) {
        this.moneda = value;
    }

    /**
     * Gets the value of the codigoRecaudacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoRecaudacion() {
        return codigoRecaudacion;
    }

    /**
     * Sets the value of the codigoRecaudacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoRecaudacion(String value) {
        this.codigoRecaudacion = value;
    }

    /**
     * Gets the value of the descripcionRecaudacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescripcionRecaudacion() {
        return descripcionRecaudacion;
    }

    /**
     * Sets the value of the descripcionRecaudacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescripcionRecaudacion(String value) {
        this.descripcionRecaudacion = value;
    }

    /**
     * Gets the value of the fechaVencimiento property.
     * 
     */
    public int getFechaVencimiento() {
        return fechaVencimiento;
    }

    /**
     * Sets the value of the fechaVencimiento property.
     * 
     */
    public void setFechaVencimiento(int value) {
        this.fechaVencimiento = value;
    }

    /**
     * Gets the value of the horaVencimiento property.
     * 
     */
    public int getHoraVencimiento() {
        return horaVencimiento;
    }

    /**
     * Sets the value of the horaVencimiento property.
     * 
     */
    public void setHoraVencimiento(int value) {
        this.horaVencimiento = value;
    }

    /**
     * Gets the value of the codigoProducto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoProducto() {
        return codigoProducto;
    }

    /**
     * Sets the value of the codigoProducto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoProducto(String value) {
        this.codigoProducto = value;
    }

    /**
     * Gets the value of the planillas property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfDPlanilla }
     *     
     */
    public ArrayOfDPlanilla getPlanillas() {
        return planillas;
    }

    /**
     * Sets the value of the planillas property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfDPlanilla }
     *     
     */
    public void setPlanillas(ArrayOfDPlanilla value) {
        this.planillas = value;
    }

}
