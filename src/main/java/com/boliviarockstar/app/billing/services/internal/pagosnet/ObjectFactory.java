
package com.boliviarockstar.app.billing.services.internal.pagosnet;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ch.swissbytes.fundempresa.billing.services.internal.pagosnet package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ch.swissbytes.fundempresa.billing.services.internal.pagosnet
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CmeRegistroItem }
     * 
     */
    public CmeRegistroItem createCmeRegistroItem() {
        return new CmeRegistroItem();
    }

    /**
     * Create an instance of {@link DatosItem }
     * 
     */
    public DatosItem createDatosItem() {
        return new DatosItem();
    }

    /**
     * Create an instance of {@link CmeRegistroPlanResponse }
     * 
     */
    public CmeRegistroPlanResponse createCmeRegistroPlanResponse() {
        return new CmeRegistroPlanResponse();
    }

    /**
     * Create an instance of {@link RespPlanilla }
     * 
     */
    public RespPlanilla createRespPlanilla() {
        return new RespPlanilla();
    }

    /**
     * Create an instance of {@link CmeRegistroPlan }
     * 
     */
    public CmeRegistroPlan createCmeRegistroPlan() {
        return new CmeRegistroPlan();
    }

    /**
     * Create an instance of {@link DatosPlanilla }
     * 
     */
    public DatosPlanilla createDatosPlanilla() {
        return new DatosPlanilla();
    }

    /**
     * Create an instance of {@link CmeRegistroItemResponse }
     * 
     */
    public CmeRegistroItemResponse createCmeRegistroItemResponse() {
        return new CmeRegistroItemResponse();
    }

    /**
     * Create an instance of {@link RespItem }
     * 
     */
    public RespItem createRespItem() {
        return new RespItem();
    }

    /**
     * Create an instance of {@link ArrayOfDPlanilla }
     * 
     */
    public ArrayOfDPlanilla createArrayOfDPlanilla() {
        return new ArrayOfDPlanilla();
    }

    /**
     * Create an instance of {@link DPlanilla }
     * 
     */
    public DPlanilla createDPlanilla() {
        return new DPlanilla();
    }

    /**
     * Create an instance of {@link DItem }
     * 
     */
    public DItem createDItem() {
        return new DItem();
    }

    /**
     * Create an instance of {@link ArrayOfDItem }
     * 
     */
    public ArrayOfDItem createArrayOfDItem() {
        return new ArrayOfDItem();
    }

}
