package com.boliviarockstar.app.billing.services.internal.pagosnet;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class PagoDto {

    private String username;
    private String emailCliente;
    private BigDecimal montoTotal;
    private String nitFacturar;
    private String nombreFacturar;
    private String codRecaudacion;

}
