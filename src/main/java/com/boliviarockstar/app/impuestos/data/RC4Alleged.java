package com.boliviarockstar.app.impuestos.data;

public class RC4Alleged {

    public String encryptRC4(String message, String key) {
        int state[] = new int[256];
        int index1 = 0;
        int index2 = 0;
        int x = 0;
        int y = 0;
        int nMen;
        String cipher = "";
        for (int i = 0; i < 256; i++) {
            state[i] = i;
        }
        for (int o = 0; o < 256; o++) {
            index2 = (getAsciiValue(key.toCharArray()[index1]) + state[o] + index2) % 256;
            int aux;
            // exchanged values
            aux = state[o];
            state[o] = state[index2];
            state[index2] = aux;
            index1 = (index1 + 1) % key.length();
        }
        int one;
        int two;
        for (int u = 0; u < message.length(); u++) {
            x = (x + 1) % 256;
            y = (state[x] + y) % 256;
            // exchanged value
            int aux2;
            aux2 = state[x];
            state[x] = state[y];
            state[y] = aux2;
            one = getAsciiValue(message.toCharArray()[u]);
            two = state[(state[x] + state[y]) % 256];
            nMen = one ^ two;
            cipher = cipher + "-" + fillZero(decimalToHexadecimal(nMen));

        }
        String result;
        result = cipher.substring(1, cipher.length());
        return result;
    }

    public String encryptRC4WithoutDash(final String message, final String key) {
        String result = encryptRC4(message, key);
        result = result.replace("-", "");
        return result;
    }

    private String decimalToHexadecimal(int valor) {
        return Integer.toHexString(valor).toUpperCase();
    }

    private int getAsciiValue(char valor) {
        return (int) valor;
    }

    private String fillZero(String valor) {
        if (valor.length() == 1) {
            valor = "0" + valor;
        }
        return valor;
    }

}

