package com.boliviarockstar.app.impuestos.data.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by jose on 22/04/2016.
 */
@Getter
@Setter
public class ControlEntityDto {

    private String authorizationStr;
    private String invoiceNumberStr;
    private String clientNit;
    private String dateStr;
    private String amountStr;
    private String dosageKey;

}
