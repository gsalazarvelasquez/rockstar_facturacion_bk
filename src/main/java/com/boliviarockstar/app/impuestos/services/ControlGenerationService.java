package com.boliviarockstar.app.impuestos.services;

import com.boliviarockstar.app.impuestos.data.dto.ControlEntityDto;

import java.text.ParseException;

/**
 * Created by jose on 22/04/2016.
 */
public interface ControlGenerationService {

    String generate(ControlEntityDto control) throws ParseException;

}
