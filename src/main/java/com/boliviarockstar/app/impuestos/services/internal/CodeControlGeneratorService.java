package com.boliviarockstar.app.impuestos.services.internal;

import com.boliviarockstar.app.impuestos.data.Base64;
import com.boliviarockstar.app.impuestos.data.RC4Alleged;
import com.boliviarockstar.app.impuestos.data.Verhoeff;
import com.boliviarockstar.app.impuestos.data.dto.ControlEntityDto;
import com.boliviarockstar.app.impuestos.services.ControlGenerationService;
import org.apache.commons.lang3.CharUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * It generates a code control using data about invoice like : invoice number,
 * client, nit, authorization , etc. for it uses three algorithms
 * "verhoeff, base64 & "
 *
 * @author Alvaro Cardozo.
 */
public class CodeControlGeneratorService implements Serializable, ControlGenerationService {

    private static final Logger LOG = Logger.getLogger(CodeControlGeneratorService.class.getName());

    /**
     * Generates code Control, It is based on process explained by
     * "Impuestos Nacionales" according six steps using three algorithms
     * (Verhoeff,base64 & RC4Alleged) throughout the process.
     * <p>
     * authorizationStr Number of authorization
     * invoiceNumberStr Number of invoice
     * clientNit        Nit of client it client doesn't have nit it must be "0"
     * dateStr          Date of registration about invoice using this format
     * YYYYMMdd
     * amountStr        amount on invoice
     * dosageKey        this String is given by "Impuestos Nacionales"
     *
     * @throws ParseException
     */
    public String generate(ControlEntityDto control)
            throws ParseException {
        String authorizationStr = control.getAuthorizationStr();
        String invoiceNumberStr = control.getInvoiceNumberStr();
        String clientNit = control.getClientNit();
        String dateStr = control.getDateStr();
        String amountStr = control.getAmountStr();
        String dosageKey = control.getDosageKey();

        LOG.log(Level.INFO, String.format("authorization=%s, invoiceNumber=%s, clientNit=%s date=%s, amount=%s, dosageKey=%s",
                authorizationStr, invoiceNumberStr, clientNit, dateStr, amountStr, dosageKey));

        final Long amountInt = roundAmount(amountStr);
        final String amount = amountInt.toString();
        Verhoeff verhoeff = new Verhoeff();

        // FIRST STEP
        final String invoiceNumberVerhoeff = generateVerhoeffNumberAndAddIt(invoiceNumberStr, 2, verhoeff);
        final String nitVerhoeff = generateVerhoeffNumberAndAddIt(clientNit, 2, verhoeff);
        final String dateVerhoeff = generateVerhoeffNumberAndAddIt(dateStr, 2, verhoeff);
        final String amountVerhoeff = generateVerhoeffNumberAndAddIt(amount, 2, verhoeff);

        final Long total = Long.valueOf(invoiceNumberVerhoeff) + Long.valueOf(nitVerhoeff) + Long.valueOf(dateVerhoeff) + Long.valueOf(amountVerhoeff);

        final String totalStr = generateVerhoeffNumberAndAddIt(total.toString(), 5, verhoeff);
        final String verhoeffDigits = StringUtils.difference(total.toString(), totalStr);

        // SECOND STEP
        final String dosageMerged = mixDosage(verhoeffDigits, dosageKey, authorizationStr, invoiceNumberVerhoeff, nitVerhoeff, dateVerhoeff, amountVerhoeff);
        // THIRD STEP
        final String rc4 = new RC4Alleged().encryptRC4WithoutDash(dosageMerged, dosageKey + verhoeffDigits);
        // FOURTH STEP & FIFTH STEP
        final String base64 = new Base64().encode(summation(verhoeffDigits, rc4));
        // SIXTH STEP
        return new RC4Alleged().encryptRC4(base64, dosageKey + verhoeffDigits);
    }

    public Long roundAmount(final String amount) throws ParseException {
        return Math.round(NumberFormat.getInstance(Locale.getDefault()).parse(amount).doubleValue());
    }

    private Long summation(final String verhoeffDigits, final String rc4) {
        LOG.log(Level.INFO, String.format("verhoeffDigits=%s, rc4=%s", verhoeffDigits, rc4));

        int st = 0;
        int s1 = 0;
        int s2 = 0;
        int s3 = 0;
        int s4 = 0;
        int s5 = 0;
        for (int i = 0; i < rc4.length(); i++) {
            st += rc4.charAt(i);
            int index = i + 1;
            if (index == 1 || (index - 1) % 5 == 0) {
                s1 += rc4.charAt(i);
            }
            if (index == 2 || (index - 2) % 5 == 0) {
                s2 += rc4.charAt(i);
            }
            if (index == 3 || (index - 3) % 5 == 0) {
                s3 += rc4.charAt(i);
            }
            if (index == 4 || (index - 4) % 5 == 0) {
                s4 += rc4.charAt(i);
            }
            if (index == 5 || (index - 5) % 5 == 0) {
                s5 += rc4.charAt(i);
            }
        }
        // truncating result
        long a1 = (st * s1) / (Integer.valueOf(verhoeffDigits.substring(0, 1)) + 1);
        long a2 = (st * s2) / (Integer.valueOf(verhoeffDigits.substring(1, 2)) + 1);
        long a3 = (st * s3) / (Integer.valueOf(verhoeffDigits.substring(2, 3)) + 1);
        long a4 = (st * s4) / (Integer.valueOf(verhoeffDigits.substring(3, 4)) + 1);
        long a5 = (st * s5) / (Integer.valueOf(verhoeffDigits.substring(4, 5)) + 1);

        return a1 + a2 + a3 + a4 + a5;
    }

    private String mixDosage(final String verhoeffDigits, final String dosageKey, final String authorizationStr, final String invoiceNumberVerhoeff, final String nitVerhoeff,
                             final String dateVerhoeff, final String amountVerhoeff) {

        LOG.log(Level.INFO, String.format("verhoeffDigits=%s, dosageKey=%s, authorization=%s, invoiceNumber=%s, " +
                        "nit=%s, date=%s, amount=%s", verhoeffDigits, dosageKey, authorizationStr, invoiceNumberVerhoeff,
                nitVerhoeff, dateVerhoeff, amountVerhoeff));

        int currentIndex = 0;
        StringBuilder dosageMerged = new StringBuilder("");
        for (int i = 0; i < verhoeffDigits.length(); i++) {
            final Integer length = Integer.valueOf(CharUtils.toString(verhoeffDigits.charAt(i))) + 1;
            final String part = dosageKey.substring(currentIndex, currentIndex + length);
            currentIndex = currentIndex + length;
            if (i == 0) {// Authorization
                dosageMerged.append(authorizationStr).append(part);
            } else if (i == 1) {// number
                dosageMerged.append(invoiceNumberVerhoeff).append(part);
            } else if (i == 2) {// nit
                dosageMerged.append(nitVerhoeff).append(part);
            } else if (i == 3) {// date
                dosageMerged.append(dateVerhoeff).append(part);
            } else if (i == 4) {// amount
                dosageMerged.append(amountVerhoeff).append(part);
            }

        }
        return dosageMerged.toString();
    }

    /**
     * Takes the base then generateOrders a verhoeff number then adds this number to
     * the base over and over and over
     */
    private String generateVerhoeffNumberAndAddIt(final String base, final Integer times, final Verhoeff verhoeff) {
        LOG.log(Level.INFO, String.format("base=%s, times=%s, verhoeff=%s", base, times, verhoeff));

        String result = base;
        for (int i = 0; i < times; i++) {
            result = result + Integer.toString(verhoeff.getVerhoeff(result));
        }
        return result;
    }
}
