package com.boliviarockstar.app.ventanilla.data.dbmigration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;


public class DBMigrationTemplate {
    private static final Logger log = LoggerFactory.getLogger(DBMigrationTemplate.class);

    /**
     * Drops an existing table.
     *
     * @param connection
     * @param tableName name of the table in Data Base
     * @throws Throwable
     * @author Miguel Ordonez
     */

    public void dropTableIfExist(final Connection connection, final String tableName) throws Throwable {
        if (relationshipExists(connection, tableName)) {
            String sql = String.format("DROP TABLE %s;", tableName);
            connection.createStatement().executeUpdate(sql);
            log.trace(sql);
        }
    }

    /**
     * Drops an existing index.
     *
     * @param connection
     * @param tableName
     * @throws Throwable
     * @author Miguel Ordonez
     */
    public void dropIndexIfExist(final Connection connection, final String tableName) throws Throwable {
        if (relationshipExists(connection, tableName)) {
            String sql = String.format("DROP INDEX %s;", tableName);
            connection.createStatement().executeUpdate(sql);
            log.trace(sql);
        }
    }

    /**
     * WARNING: THIS METHOD IS OPTIMISED TO WORK WITH POSTGRESQL
     *
     * @param connection
     * @param tableName
     * @return
     * @throws Throwable
     * @author Miguel Ordonez
     */
    public boolean relationshipExists(final Connection connection, final String tableName) throws Throwable {
        String sql = "SELECT count(relname) FROM pg_class WHERE relname = ?;";
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        int count = 0;
        if (rs.next())
            count = rs.getInt(0);
        return count > 0;
    }

    /**
     * @param connection
     * @param tableName
     * @return
     * @throws Throwable
     * @author Miguel Ordonez
     */
    public boolean tableExists(final Connection connection, final String tableName) throws Throwable {
        return relationshipExists(connection, tableName);
    }

    /**
     * WARNING: THIS METHOD IS OPTIMISED TO WORK WITH POSTGRESQL
     *
     * @param connection
     * @param tableName
     * @param constraintName
     * @return
     * @throws Throwable
     * @author Miguel Ordonez
     */
    public boolean constraintExists(final Connection connection, final String tableName, final String constraintName) throws Throwable {
        String sql = "SELECT count(c.conname) FROM pg_constraint c, pg_class cl WHERE cl.oid=c.conrelid AND c.contype='f' AND c.conname=? AND cl.relname = ?;";
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        int count = 0;
        if (rs.next())
            count = rs.getInt(0);
        return count > 0;
    }

    /**
     * Drops an existing sequence.
     *
     * @param connection
     * @param name of sequence in Data Base
     * @throws Throwable
     * @author Miguel Ordonez
     */
    public void dropSequenceIfExist(final Connection connection, final String name) throws Throwable {
        if (relationshipExists(connection, name)) {
            String sql = String.format("DROP SEQUENCE %s;", name);
            connection.createStatement().executeUpdate(sql);
            log.trace(sql);
        }
    }

    /**
     * Checks if given table has given column
     * WARNING: THIS METHOD IS OPTIMISED TO WORK WITH POSTGRESQL
     *
     * @param connection
     * @param tableName
     * @param columnName
     * @return
     * @throws Throwable
     * @author Miguel Ordonez
     */
    public boolean columnExists(final Connection connection, final String tableName, final String columnName) throws Throwable {
        String sql = "SELECT count(attname) FROM pg_attribute WHERE attrelid = (SELECT oid FROM pg_class WHERE relname = ?) AND attname = ?;";
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        int count = 0;
        if (rs.next())
            count = rs.getInt(0);
        return count > 0;
    }

    /**
     * Drops an existing table-column.
     *
     * @param connection
     * @param tableName = name of table in Data Base
     * @param columnName = name of column to be deleted
     * @throws Throwable
     * @author Miguel Ordonez
     */
    public void dropColumnFromTableIfExist(final Connection connection, final String tableName, final String columnName) throws Throwable {
        if (columnExists(connection, tableName, columnName)) {
            String sql = String.format("ALTER TABLE %s DROP COLUMN %s; ", tableName, columnName);
            connection.createStatement().executeUpdate(sql);
            log.trace(sql);
        }
    }

    /**
     * Add column in table
     *
     * @param connection
     * @param tableName
     * @param columnName
     * @param type
     * @param defaultValue
     * @param allowNullValue
     * @throws Throwable
     * @author Miguel Ordonez
     */
    public void addColumnToTable(final Connection connection, final String tableName, final String columnName, final String type, final String defaultValue, final boolean allowNullValue)
            throws Throwable {
        if (!columnExists(connection, tableName, columnName)) {
            final String allowNull = allowNullValue ? " " : " NOT NULL ";
            final String defVal = defaultValue != null ? "DEFAULT " + defaultValue : "";
            String sql = String.format("ALTER TABLE %s ADD COLUMN %s %s%s%s; ", tableName, columnName, type, allowNull, defVal);
            connection.createStatement().executeUpdate(sql);
            log.trace(sql);
        }
    }

    /**
     * Add column an existing table if not exist column.
     *
     * @param connection
     * @param tableName = name of table at Data Base
     * @param columnName = name of column to be added
     * @param type = type of column added
     * @param defaultValue = default data
     * @throws Throwable
     * @author Miguel Ordonez
     */
    public void addColumnToTableIfNotExist(final Connection connection, final String tableName, final String columnName, final String type, final String defaultValue) throws Throwable {
        if (!columnExists(connection, tableName, columnName)) {
            String sql = String.format("ALTER TABLE %s ADD COLUMN %s %s DEFAULT %s; ", tableName, columnName, type, defaultValue);
            connection.createStatement().executeUpdate(sql);
            log.trace(sql);
        }
    }

    public void createTableIfNotPresent(final Connection connection, final String tableName, final String sqlStatement) throws Throwable {
        if (!tableExists(connection, tableName)) {
            connection.createStatement().executeUpdate(sqlStatement);
            log.trace(sqlStatement);
        }
    }

    /**
     * Add foreign key an existing table if not exist foreign key.
     *
     * @param connection
     * @param tableName = name of the table in Data Base
     * @param constraintReference = name of the column
     * @param constraintName = name of the constraint to be added
     * @param tableReference = name of the referenced table by fk
     * @param columnReference = column name of the referenced table by fk
     * @throws Throwable
     * @author Miguel Ordonez
     */
    public void addConstraintToTableIfNotExist(final Connection connection, final String tableName, final String constraintReference, final String constraintName, final String tableReference,
                                               final String columnReference) throws Throwable {
        if (!constraintExists(connection, tableName, constraintName)) {
            String sql = String.format("ALTER TABLE %s ADD CONSTRAINT %s FOREIGN KEY(%s) REFERENCES %s(%s); ", tableName, constraintName, constraintReference, tableReference, columnReference);
            connection.createStatement().executeUpdate(sql);
            log.trace(sql);
        }
    }

    /**
     * Remove not null an existing table-column.
     *
     * @param connection
     * @param tb_name = name of table at Data Base
     * @pram cl_name = name of colum at table "tb_name" to be deleted
     * @throws Throwable
     * @author Miguel Ordonez
     */
    public void alterColumnOfTableIfExistRemoveNotNull(final Connection connection, final String tb_name, final String cl_name) throws Throwable {
        if (columnExists(connection, tb_name, cl_name)) {
            String sql = String.format("ALTER TABLE %s ALTER COLUMN %s DROP NOT NULL; ", tb_name, cl_name);
            connection.createStatement().executeUpdate(sql);
            log.trace(sql);
        }
    }

    /**
     * Rename column-table if the original and target column names don't exist.
     *
     * @param connection
     * @param tableName
     * @param columnName
     * @param columnReName
     * @throws Throwable
     * @author Miguel Ordonez
     */
    public void renameColumn(final Connection connection, final String tableName, final String columnName, final String columnReName) throws Throwable {
        if (columnExists(connection, tableName, columnName) && !columnExists(connection, tableName, columnReName)) {
            String sql = String.format("ALTER TABLE %s RENAME COLUMN %s TO %s; ", tableName, columnName, columnReName);
            connection.createStatement().executeUpdate(sql);
            log.trace(sql);
        }
    }
}
