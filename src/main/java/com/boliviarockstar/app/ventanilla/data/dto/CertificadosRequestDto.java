package com.boliviarockstar.app.ventanilla.data.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * Created by jose on 22/03/2016.
 */
@Getter
@Setter
public class CertificadosRequestDto extends TramiteRequestDto implements Serializable {

    private String matricula;

    //certificado especial
    private String certificado;
    private String ejemplares;
    private String descripcion;
    private String sucursal;

    //certificado en linea
    private List<CertificadoDto> documentos;

    //validacion
    private long fechaCertificado;
    private String usoValidacion;
    private String codigoValidacion;
    private String solicitante;
    private String ci;
    private String expci;
    private String email;
}