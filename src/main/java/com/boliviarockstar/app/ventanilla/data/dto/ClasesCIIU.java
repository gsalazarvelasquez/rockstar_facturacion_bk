package com.boliviarockstar.app.ventanilla.data.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by jose on 29/04/2016.
 */
@Getter
@Setter
public class ClasesCIIU {

    private String id;
    private String idSeccion;
    private String descripcion;

}
