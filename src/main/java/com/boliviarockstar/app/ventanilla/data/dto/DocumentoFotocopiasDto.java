package com.boliviarockstar.app.ventanilla.data.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by jose on 22/03/2016.
 */
@Getter
@Setter
public class DocumentoFotocopiasDto {

    private String idLibro;
    private String numRegistro;
    private String descripcion;

    @Override
    public String toString(){
        String result = "idLibro=" + idLibro
                + "-numRegistro=" + numRegistro
                + "-descripcion=" + descripcion;

        return result;
    }
}
