package com.boliviarockstar.app.ventanilla.data.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by jose on 23/03/2016.
 */
@Getter
@Setter
public class EntidadesDto {

    private String codError;
    private String descError;
    private String ultimoId;
    private List<EntidadDto> entidades;
}
