package com.boliviarockstar.app.ventanilla.data.dto;

import com.boliviarockstar.app.ventanilla.data.model.Homonimia;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by daniel on 31/03/2016.
 */
@Getter
@Setter
public class HomonimiaResponseDto extends TramiteResponseDto {

    private String fechaActualizacion;

    public HomonimiaResponseDto(){

    }

    public HomonimiaResponseDto(Homonimia homonimia){
        this.setPkTramite(homonimia.getPkHomonimia());
        this.setCodigoTramite(homonimia.getCodigoTramite());

        this.setPinQuiosco(homonimia.getPinQuiosco());
        this.setCodigoError(homonimia.getCodigoError());
        this.setMensajeError(homonimia.getMensajeError());
        this.setEstadoTramite(homonimia.getEstadoTramite());
        this.setRequestXml(homonimia.getRequestXml());

        this.setResponseXml(homonimia.getResponseXml());
    }
}
