package com.boliviarockstar.app.ventanilla.data.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by daniel on 29/03/2016.
 */
@Getter
@Setter
public class JsonResult implements Serializable {

    private boolean success;
    private Object data;
    private String message;

    public JsonResult() {
    }

    public JsonResult(boolean success, Object data, String message){
        this.success = success;
        this.data = data;
        this.message = message;
    }
}
