package com.boliviarockstar.app.ventanilla.data.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by jose on 28/03/2016.
 */
@Getter
@Setter
public class PerfilAdabasDto {

    private String codigo;
    private String descripcion;
}
