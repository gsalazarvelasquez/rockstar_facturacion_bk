package com.boliviarockstar.app.ventanilla.data.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by daniel on 31/03/2016.
 */
@Getter
@Setter
public class PreciosResponseDto extends TramiteResponseDto {

    private String arancel;
}
