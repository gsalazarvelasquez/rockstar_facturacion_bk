package com.boliviarockstar.app.ventanilla.data.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@ToString
@Setter
@Getter
public class ResponseDto {

    //tipo de tramite
    private int tramite;
    private String descTramite;

    //request-response
    private String request;
    private String response;
    private String ultimoId;

    //codigos de error-respuesta
    private String codError;
    private String descError;

    //consulta de precio
    private String arancel;

    //homonimia re/ingresar-estado
    private String codigoTramite;
    private String pinQuiosco;
    private String fechaActualizacion;
    private String estadoHomonimia;

    //Fotocopias listado
    private List<DocumentoFotocopiasDto> documentos;
}
