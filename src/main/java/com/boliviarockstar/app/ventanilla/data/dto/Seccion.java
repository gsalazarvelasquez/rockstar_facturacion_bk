package com.boliviarockstar.app.ventanilla.data.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by jose on 29/04/2016.
 */
@Getter
@Setter
public class Seccion {

    private String id;
    private String descripcion;

}
