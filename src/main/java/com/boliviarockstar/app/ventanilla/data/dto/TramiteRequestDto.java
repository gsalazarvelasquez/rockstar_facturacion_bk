package com.boliviarockstar.app.ventanilla.data.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by daniel on 28/03/2016.
 */

@Getter
@Setter
public class TramiteRequestDto implements Serializable {

    protected Long pkTramite;
    protected String userName;

    protected String payCode;
    protected String payMode;
    protected String payAmount;
//    protected String estadoTramite;

    public TramiteRequestDto(){

    }

    public TramiteRequestDto(Long pkTramite, String userName, String payCode, String payMode, String payAmount){
        this.pkTramite = pkTramite;
        this.userName = userName;
        this.payCode = payCode;
        this.payMode = payMode;
        this.payAmount = payAmount;
    }
}
