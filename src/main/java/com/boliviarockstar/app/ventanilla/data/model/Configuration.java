package com.boliviarockstar.app.ventanilla.data.model;

/**
 * Created by daniel on 16/05/2016.
 */
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "configuration")
public class Configuration implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "key_string")
    private String key_string;

    @Column(name = "value_string")
    private String value_string;

    public Configuration() {
    }

    public Configuration(String key_string, String value_string) {
        this.key_string = key_string;
        this.value_string = value_string;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey_string() {
        return this.key_string;
    }

    public void setKey_string(String key_string) {
        this.key_string = key_string;
    }

    public String getValue_string() {
        return this.value_string;
    }

    public void setValue_string(String value_string) {
        this.value_string = value_string;
    }

    @Override
    public boolean equals(Object other) {
        if ((this == other))
            return true;
        if ((other == null))
            return false;
        if (!(other instanceof Configuration))
            return false;
        Configuration castOther = (Configuration) other;
        return ((this.getId() == castOther.getId()) || (this.getId() != null
                && castOther.getId() != null && this.getId().equals(
                castOther.getId())));
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 37 * result + (getId() == null ? 0 : this.getId().hashCode());
        return result;
    }
}
