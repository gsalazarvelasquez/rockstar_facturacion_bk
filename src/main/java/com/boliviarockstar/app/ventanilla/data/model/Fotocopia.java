package com.boliviarockstar.app.ventanilla.data.model;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by daniel on 27/04/2016.
 */
@Entity
public class Fotocopia {
    private Long id;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private String matricula;

    @Basic
    @Column(name = "matricula", nullable = true, length = 128)
    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    private String documentos;

    @Basic
    @Column(name = "documentos", nullable = true, columnDefinition = "Text")
    public String getDocumentos() {
        return documentos;
    }

    public void setDocumentos(String documentos) {
        this.documentos = documentos;
    }

    private String descripcion;

    @Basic
    @Column(name = "descripcion", nullable = true, length = 1024)
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    private String sucursal;

    @Basic
    @Column(name = "sucursal", nullable = true, length = 1024)
    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    private String userName;

    @Basic
    @Column(name = "user_name", nullable = false, length = 128)
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    private String requestXml;

    @Basic
    @Column(name = "request_xml", nullable = true, columnDefinition = "Text")
    public String getRequestXml() {
        return requestXml;
    }

    public void setRequestXml(String requestXml) {
        this.requestXml = requestXml;
    }

    private String responseXml;

    @Basic
    @Column(name = "response_xml", nullable = true, columnDefinition = "Text")
    public String getResponseXml() {
        return responseXml;
    }

    public void setResponseXml(String responseXml) {
        this.responseXml = responseXml;
    }

    private String payCode;

    @Basic
    @Column(name = "pay_code", nullable = true, length = 1024)
    public String getPayCode() {
        return payCode;
    }

    public void setPayCode(String payCode) {
        this.payCode = payCode;
    }

    private String payMode;

    @Basic
    @Column(name = "pay_mode", nullable = true, length = 128)
    public String getPayMode() {
        return payMode;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    private String payAmount;

    @Basic
    @Column(name = "pay_amount", nullable = true, length = 256)
    public String getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(String payAmount) {
        this.payAmount = payAmount;
    }

    private String codigoTramite;

    @Basic
    @Column(name = "codigo_tramite", nullable = true, length = 128)
    public String getCodigoTramite() {
        return codigoTramite;
    }

    public void setCodigoTramite(String codigoTramite) {
        this.codigoTramite = codigoTramite;
    }

    private String codigoError;

    @Basic
    @Column(name = "codigo_error", nullable = true, length = 128)
    public String getCodigoError() {
        return codigoError;
    }

    public void setCodigoError(String codigoError) {
        this.codigoError = codigoError;
    }

    private String mensajeError;

    @Basic
    @Column(name = "mensaje_error", nullable = true, length = 1024)
    public String getMensajeError() {
        return mensajeError;
    }

    public void setMensajeError(String mensajeError) {
        this.mensajeError = mensajeError;
    }

    private String estadoTramite;

    @Basic
    @Column(name = "estado_tramite", nullable = true, length = 128)
    public String getEstadoTramite() {
        return estadoTramite;
    }

    public void setEstadoTramite(String estadoTramite) {
        this.estadoTramite = estadoTramite;
    }

    private Timestamp fechaCreacion;

    @Basic
    @Column(name = "fecha_creacion", nullable = true)
    public Timestamp getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Timestamp fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Fotocopia fotocopia = (Fotocopia) o;

        if (id != null ? !id.equals(fotocopia.id) : fotocopia.id != null) return false;
        if (matricula != null ? !matricula.equals(fotocopia.matricula) : fotocopia.matricula != null) return false;
        if (documentos != null ? !documentos.equals(fotocopia.documentos) : fotocopia.documentos != null) return false;
        if (descripcion != null ? !descripcion.equals(fotocopia.descripcion) : fotocopia.descripcion != null)
            return false;
        if (sucursal != null ? !sucursal.equals(fotocopia.sucursal) : fotocopia.sucursal != null) return false;
        if (userName != null ? !userName.equals(fotocopia.userName) : fotocopia.userName != null) return false;
        if (requestXml != null ? !requestXml.equals(fotocopia.requestXml) : fotocopia.requestXml != null) return false;
        if (responseXml != null ? !responseXml.equals(fotocopia.responseXml) : fotocopia.responseXml != null)
            return false;
        if (payCode != null ? !payCode.equals(fotocopia.payCode) : fotocopia.payCode != null) return false;
        if (payMode != null ? !payMode.equals(fotocopia.payMode) : fotocopia.payMode != null) return false;
        if (payAmount != null ? !payAmount.equals(fotocopia.payAmount) : fotocopia.payAmount != null) return false;
        if (codigoTramite != null ? !codigoTramite.equals(fotocopia.codigoTramite) : fotocopia.codigoTramite != null)
            return false;
        if (codigoError != null ? !codigoError.equals(fotocopia.codigoError) : fotocopia.codigoError != null)
            return false;
        if (mensajeError != null ? !mensajeError.equals(fotocopia.mensajeError) : fotocopia.mensajeError != null)
            return false;
        if (estadoTramite != null ? !estadoTramite.equals(fotocopia.estadoTramite) : fotocopia.estadoTramite != null)
            return false;
        if (fechaCreacion != null ? !fechaCreacion.equals(fotocopia.fechaCreacion) : fotocopia.fechaCreacion != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (matricula != null ? matricula.hashCode() : 0);
        result = 31 * result + (documentos != null ? documentos.hashCode() : 0);
        result = 31 * result + (descripcion != null ? descripcion.hashCode() : 0);
        result = 31 * result + (sucursal != null ? sucursal.hashCode() : 0);
        result = 31 * result + (userName != null ? userName.hashCode() : 0);
        result = 31 * result + (requestXml != null ? requestXml.hashCode() : 0);
        result = 31 * result + (responseXml != null ? responseXml.hashCode() : 0);
        result = 31 * result + (payCode != null ? payCode.hashCode() : 0);
        result = 31 * result + (payMode != null ? payMode.hashCode() : 0);
        result = 31 * result + (payAmount != null ? payAmount.hashCode() : 0);
        result = 31 * result + (codigoTramite != null ? codigoTramite.hashCode() : 0);
        result = 31 * result + (codigoError != null ? codigoError.hashCode() : 0);
        result = 31 * result + (mensajeError != null ? mensajeError.hashCode() : 0);
        result = 31 * result + (estadoTramite != null ? estadoTramite.hashCode() : 0);
        result = 31 * result + (fechaCreacion != null ? fechaCreacion.hashCode() : 0);
        return result;
    }
}
