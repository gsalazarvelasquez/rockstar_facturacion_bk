package com.boliviarockstar.app.ventanilla.data.model;

import javax.persistence.*;

/**
 * Created by jose on 31/03/2016.
 */
@Entity
@Table(name = "fotocopia_tipo_tramite", schema = "", catalog = "fundempresa_local")
public class FotocopiaTipoTramite {
    private Long id;
    private Fotocopia fotocopiaByIdFotocopia;

    @Id
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FotocopiaTipoTramite that = (FotocopiaTipoTramite) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @ManyToOne
    @JoinColumn(name = "id_fotocopia", referencedColumnName = "id", nullable = false)
    public Fotocopia getFotocopiaByIdFotocopia() {
        return fotocopiaByIdFotocopia;
    }

    public void setFotocopiaByIdFotocopia(Fotocopia fotocopiaByIdFotocopia) {
        this.fotocopiaByIdFotocopia = fotocopiaByIdFotocopia;
    }
}
