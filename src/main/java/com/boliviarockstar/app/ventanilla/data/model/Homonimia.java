package com.boliviarockstar.app.ventanilla.data.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@Entity
public class Homonimia {
    @Id
    @Column(name= "pk_homonimia", nullable= false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long pkHomonimia;

    @Column(name = "fk_tipo_societario", nullable = false)
    private Long tipoSocietario;

    @Column(name = "user_name", nullable = false, length = 128)
    private String userName;

    @Column(name = "opcion1", nullable = true, length = 512)
    private String opcion1;

    @Column(name = "opcion2", nullable = true, length = 512)
    private String opcion2;

    @Column(name = "opcion3", nullable = true, length = 512)
    private String opcion3;

    @Column(name = "actividad_economica", nullable = true, length = 4096)
    private String actividadEconomica;

    @Column(name = "precio", nullable = true, length = 256)
    private String precio;

    @Column(name = "pay_code", nullable = true, length = 1024)
    private String payCode;

    @Column(name = "pay_mode", nullable = true, length = 128)
    private String payMode;

    @Column(name = "request_xml", nullable = true, columnDefinition = "Text")
    private String requestXml;

    @Column(name = "response_xml", nullable = true, columnDefinition = "Text")
    private String responseXml;

    @Column(name = "codigo_error", nullable = true, length = 128)
    private String codigoError;

    @Column(name = "mensaje_error", nullable = true, length = 1024)
    private String mensajeError;

    @Column(name = "pin_quiosco", nullable = true, length = 128)
    private String pinQuiosco;

    @Column(name = "codigo_tramite", nullable = true, length = 128)
    private String codigoTramite;

    @Column(name = "tipo_ingreso", nullable = true, length = 128)
    private String tipoIngreso;

    @Column(name = "estado_tramite", nullable = true, length = 128)
    private String estadoTramite;

    @Column(name = "fecha_creacion", nullable = true)
    private Timestamp fechaCreacion;

    @Column(name = "fecha_ingreso", nullable = true)
    private Timestamp fechaIngreso;

    @Column(name = "fecha_actualizacion", nullable = true)
    private Timestamp fechaActualizacion;

//    @JoinColumn(name = "fk_tipo_societario", referencedColumnName = "id")
//    @ManyToOne(fetch = FetchType.EAGER)
//    private TipoSociedad tipoSocietario;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Homonimia homonimia = (Homonimia) o;

        if (pkHomonimia != null ? !pkHomonimia.equals(homonimia.pkHomonimia) : homonimia.pkHomonimia != null)
            return false;
        if (userName != null ? !userName.equals(homonimia.userName) : homonimia.userName != null) return false;
        if (opcion1 != null ? !opcion1.equals(homonimia.opcion1) : homonimia.opcion1 != null) return false;
        if (opcion2 != null ? !opcion2.equals(homonimia.opcion2) : homonimia.opcion2 != null) return false;
        if (opcion3 != null ? !opcion3.equals(homonimia.opcion3) : homonimia.opcion3 != null) return false;
        if (actividadEconomica != null ? !actividadEconomica.equals(homonimia.actividadEconomica) : homonimia.actividadEconomica != null)
            return false;
        if (precio != null ? !precio.equals(homonimia.precio) : homonimia.precio != null) return false;
        if (payCode != null ? !payCode.equals(homonimia.payCode) : homonimia.payCode != null) return false;
        if (payMode != null ? !payMode.equals(homonimia.payMode) : homonimia.payMode != null) return false;
        if (responseXml != null ? !responseXml.equals(homonimia.responseXml) : homonimia.responseXml != null)
            return false;
        if (codigoError != null ? !codigoError.equals(homonimia.codigoError) : homonimia.codigoError != null)
            return false;
        if (mensajeError != null ? !mensajeError.equals(homonimia.mensajeError) : homonimia.mensajeError != null)
            return false;
        if (pinQuiosco != null ? !pinQuiosco.equals(homonimia.pinQuiosco) : homonimia.pinQuiosco != null) return false;
        if (codigoTramite != null ? !codigoTramite.equals(homonimia.codigoTramite) : homonimia.codigoTramite != null)
            return false;
        if (tipoIngreso != null ? !tipoIngreso.equals(homonimia.tipoIngreso) : homonimia.tipoIngreso != null)
            return false;
        if (estadoTramite != null ? !estadoTramite.equals(homonimia.estadoTramite) : homonimia.estadoTramite != null)
            return false;
        if (fechaCreacion != null ? !fechaCreacion.equals(homonimia.fechaCreacion) : homonimia.fechaCreacion != null)
            return false;
        if (fechaIngreso != null ? !fechaIngreso.equals(homonimia.fechaIngreso) : homonimia.fechaIngreso != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = pkHomonimia != null ? pkHomonimia.hashCode() : 0;
        result = 31 * result + (userName != null ? userName.hashCode() : 0);
        result = 31 * result + (opcion1 != null ? opcion1.hashCode() : 0);
        result = 31 * result + (opcion2 != null ? opcion2.hashCode() : 0);
        result = 31 * result + (opcion3 != null ? opcion3.hashCode() : 0);
        result = 31 * result + (actividadEconomica != null ? actividadEconomica.hashCode() : 0);
        result = 31 * result + (precio != null ? precio.hashCode() : 0);
        result = 31 * result + (payCode != null ? payCode.hashCode() : 0);
        result = 31 * result + (payMode != null ? payMode.hashCode() : 0);
        result = 31 * result + (responseXml != null ? responseXml.hashCode() : 0);
        result = 31 * result + (codigoError != null ? codigoError.hashCode() : 0);
        result = 31 * result + (mensajeError != null ? mensajeError.hashCode() : 0);
        result = 31 * result + (pinQuiosco != null ? pinQuiosco.hashCode() : 0);
        result = 31 * result + (codigoTramite != null ? codigoTramite.hashCode() : 0);
        result = 31 * result + (tipoIngreso != null ? tipoIngreso.hashCode() : 0);
        result = 31 * result + (estadoTramite != null ? estadoTramite.hashCode() : 0);
        result = 31 * result + (fechaCreacion != null ? fechaCreacion.hashCode() : 0);
        result = 31 * result + (fechaIngreso != null ? fechaIngreso.hashCode() : 0);
        return result;
    }
}
