package com.boliviarockstar.app.ventanilla.data.model;

import javax.persistence.*;

/**
 * Created by jose on 03/06/2016.
 */
@Entity
@javax.persistence.Table(name = "listado_empresas", schema = "", catalog = "fundempresa_local")
public class ListadoEmpresas {
    private Long id;

    @Id
    @Column(name= "id", nullable= false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private String username;

    @Basic
    @javax.persistence.Column(name = "username", nullable = true, insertable = true, updatable = true, length = 100)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    private String usoInformacion;

    @Basic
    @javax.persistence.Column(name = "uso_informacion", nullable = true, insertable = true, updatable = true, length = 100)
    public String getUsoInformacion() {
        return usoInformacion;
    }

    public void setUsoInformacion(String usoInformacion) {
        this.usoInformacion = usoInformacion;
    }

    private String sucursal;

    @Basic
    @javax.persistence.Column(name = "sucursal", nullable = true, insertable = true, updatable = true, length = 100)
    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    private String payCode;

    @Basic
    @javax.persistence.Column(name = "payCode", nullable = true, insertable = true, updatable = true, length = 100)
    public String getPayCode() {
        return payCode;
    }

    public void setPayCode(String payCode) {
        this.payCode = payCode;
    }

    private String payMode;

    @Basic
    @javax.persistence.Column(name = "payMode", nullable = true, insertable = true, updatable = true, length = 100)
    public String getPayMode() {
        return payMode;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    private String payAmount;

    @Basic
    @javax.persistence.Column(name = "payAmount", nullable = true, insertable = true, updatable = true, length = 100)
    public String getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(String payAmount) {
        this.payAmount = payAmount;
    }

    private Integer todosDepartamentos;

    @Basic
    @javax.persistence.Column(name = "todos_departamentos", nullable = true, insertable = true, updatable = true)
    public Integer getTodosDepartamentos() {
        return todosDepartamentos;
    }

    public void setTodosDepartamentos(Integer todosDepartamentos) {
        this.todosDepartamentos = todosDepartamentos;
    }

    private Integer todosTiposSociedad;

    @Basic
    @javax.persistence.Column(name = "todos_tipos_sociedad", nullable = true, insertable = true, updatable = true)
    public Integer getTodosTiposSociedad() {
        return todosTiposSociedad;
    }

    public void setTodosTiposSociedad(Integer todosTiposSociedad) {
        this.todosTiposSociedad = todosTiposSociedad;
    }

    private Integer todosEstados;

    @Basic
    @javax.persistence.Column(name = "todos_estados", nullable = true, insertable = true, updatable = true)
    public Integer getTodosEstados() {
        return todosEstados;
    }

    public void setTodosEstados(Integer todosEstados) {
        this.todosEstados = todosEstados;
    }

    private Integer todasSecciones;

    @Basic
    @javax.persistence.Column(name = "todas_secciones", nullable = true, insertable = true, updatable = true)
    public Integer getTodasSecciones() {
        return todasSecciones;
    }

    public void setTodasSecciones(Integer todasSecciones) {
        this.todasSecciones = todasSecciones;
    }

    private String anhosInscritas;

    @Basic
    @javax.persistence.Column(name = "anhos_inscritas", nullable = true, insertable = true, updatable = true, length = 500)
    public String getAnhosInscritas() {
        return anhosInscritas;
    }

    public void setAnhosInscritas(String anhosInscritas) {
        this.anhosInscritas = anhosInscritas;
    }

    private String anhosCanceladas;

    @Basic
    @javax.persistence.Column(name = "anhos_canceladas", nullable = true, insertable = true, updatable = true, length = 500)
    public String getAnhosCanceladas() {
        return anhosCanceladas;
    }

    public void setAnhosCanceladas(String anhosCanceladas) {
        this.anhosCanceladas = anhosCanceladas;
    }

    private String departamentos;

    @Basic
    @javax.persistence.Column(name = "departamentos", nullable = true, insertable = true, updatable = true, length = 1048)
    public String getDepartamentos() {
        return departamentos;
    }

    public void setDepartamentos(String departamentos) {
        this.departamentos = departamentos;
    }

    private String municipios;

    @Basic
    @javax.persistence.Column(name = "municipios", nullable = true, insertable = true, updatable = true, length = 4098)
    public String getMunicipios() {
        return municipios;
    }

    public void setMunicipios(String municipios) {
        this.municipios = municipios;
    }

    private String tiposSociedad;

    @Basic
    @javax.persistence.Column(name = "tipos_sociedad", nullable = true, insertable = true, updatable = true, length = 1048)
    public String getTiposSociedad() {
        return tiposSociedad;
    }

    public void setTiposSociedad(String tiposSociedad) {
        this.tiposSociedad = tiposSociedad;
    }

    private String estados;

    @Basic
    @javax.persistence.Column(name = "estados", nullable = true, insertable = true, updatable = true, length = 1048)
    public String getEstados() {
        return estados;
    }

    public void setEstados(String estados) {
        this.estados = estados;
    }

    private String secciones;

    @Basic
    @javax.persistence.Column(name = "secciones", nullable = true, insertable = true, updatable = true, length = 2048)
    public String getSecciones() {
        return secciones;
    }

    public void setSecciones(String secciones) {
        this.secciones = secciones;
    }

    private String clasesCIiu;

    @Basic
    @javax.persistence.Column(name = "clases_cIIU", nullable = true, insertable = true, updatable = true, length = 4098)
    public String getClasesCIiu() {
        return clasesCIiu;
    }

    public void setClasesCIiu(String clasesCIiu) {
        this.clasesCIiu = clasesCIiu;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ListadoEmpresas that = (ListadoEmpresas) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (username != null ? !username.equals(that.username) : that.username != null) return false;
        if (usoInformacion != null ? !usoInformacion.equals(that.usoInformacion) : that.usoInformacion != null)
            return false;
        if (sucursal != null ? !sucursal.equals(that.sucursal) : that.sucursal != null) return false;
        if (payCode != null ? !payCode.equals(that.payCode) : that.payCode != null) return false;
        if (payMode != null ? !payMode.equals(that.payMode) : that.payMode != null) return false;
        if (payAmount != null ? !payAmount.equals(that.payAmount) : that.payAmount != null) return false;
        if (todosDepartamentos != null ? !todosDepartamentos.equals(that.todosDepartamentos) : that.todosDepartamentos != null)
            return false;
        if (todosTiposSociedad != null ? !todosTiposSociedad.equals(that.todosTiposSociedad) : that.todosTiposSociedad != null)
            return false;
        if (todosEstados != null ? !todosEstados.equals(that.todosEstados) : that.todosEstados != null) return false;
        if (todasSecciones != null ? !todasSecciones.equals(that.todasSecciones) : that.todasSecciones != null)
            return false;
        if (anhosInscritas != null ? !anhosInscritas.equals(that.anhosInscritas) : that.anhosInscritas != null)
            return false;
        if (anhosCanceladas != null ? !anhosCanceladas.equals(that.anhosCanceladas) : that.anhosCanceladas != null)
            return false;
        if (departamentos != null ? !departamentos.equals(that.departamentos) : that.departamentos != null)
            return false;
        if (municipios != null ? !municipios.equals(that.municipios) : that.municipios != null) return false;
        if (tiposSociedad != null ? !tiposSociedad.equals(that.tiposSociedad) : that.tiposSociedad != null)
            return false;
        if (estados != null ? !estados.equals(that.estados) : that.estados != null) return false;
        if (secciones != null ? !secciones.equals(that.secciones) : that.secciones != null) return false;
        if (clasesCIiu != null ? !clasesCIiu.equals(that.clasesCIiu) : that.clasesCIiu != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (usoInformacion != null ? usoInformacion.hashCode() : 0);
        result = 31 * result + (sucursal != null ? sucursal.hashCode() : 0);
        result = 31 * result + (payCode != null ? payCode.hashCode() : 0);
        result = 31 * result + (payMode != null ? payMode.hashCode() : 0);
        result = 31 * result + (payAmount != null ? payAmount.hashCode() : 0);
        result = 31 * result + (todosDepartamentos != null ? todosDepartamentos.hashCode() : 0);
        result = 31 * result + (todosTiposSociedad != null ? todosTiposSociedad.hashCode() : 0);
        result = 31 * result + (todosEstados != null ? todosEstados.hashCode() : 0);
        result = 31 * result + (todasSecciones != null ? todasSecciones.hashCode() : 0);
        result = 31 * result + (anhosInscritas != null ? anhosInscritas.hashCode() : 0);
        result = 31 * result + (anhosCanceladas != null ? anhosCanceladas.hashCode() : 0);
        result = 31 * result + (departamentos != null ? departamentos.hashCode() : 0);
        result = 31 * result + (municipios != null ? municipios.hashCode() : 0);
        result = 31 * result + (tiposSociedad != null ? tiposSociedad.hashCode() : 0);
        result = 31 * result + (estados != null ? estados.hashCode() : 0);
        result = 31 * result + (secciones != null ? secciones.hashCode() : 0);
        result = 31 * result + (clasesCIiu != null ? clasesCIiu.hashCode() : 0);
        return result;
    }
}
