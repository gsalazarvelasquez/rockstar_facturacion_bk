package com.boliviarockstar.app.ventanilla.data.model;

import javax.persistence.*;

/**
 * Created by jose on 29/04/2016.
 */
@Entity
@Table(name = "perfiles_adabas", schema = "", catalog = "fundempresa_local")
public class PerfilesAdabas {
    private Integer id;
    private String perfilAdabas;
    private Integer perfilLocal;

    @Id
    @Column(name= "id", nullable= false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "perfil_adabas", nullable = true, insertable = true, updatable = true, length = 50)
    public String getPerfilAdabas() {
        return perfilAdabas;
    }

    public void setPerfilAdabas(String perfilAdabas) {
        this.perfilAdabas = perfilAdabas;
    }

    @Basic
    @Column(name = "perfil_local", nullable = true, insertable = true, updatable = true)
    public Integer getPerfilLocal() {
        return perfilLocal;
    }

    public void setPerfilLocal(Integer perfilLocal) {
        this.perfilLocal = perfilLocal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PerfilesAdabas that = (PerfilesAdabas) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (perfilAdabas != null ? !perfilAdabas.equals(that.perfilAdabas) : that.perfilAdabas != null) return false;
        if (perfilLocal != null ? !perfilLocal.equals(that.perfilLocal) : that.perfilLocal != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (perfilAdabas != null ? perfilAdabas.hashCode() : 0);
        result = 31 * result + (perfilLocal != null ? perfilLocal.hashCode() : 0);
        return result;
    }
}
