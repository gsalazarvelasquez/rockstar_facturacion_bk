package com.boliviarockstar.app.ventanilla.data.model;

import javax.persistence.*;

/**
 * Created by jose on 29/04/2016.
 */
@Entity
@Table(name = "perfiles_local", schema = "", catalog = "fundempresa_local")
public class PerfilesLocal {
    private Integer id;
    private String descripcion;

    @Id
    @Column(name= "id", nullable= false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "descripcion", nullable = true, insertable = true, updatable = true, length = 100)
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PerfilesLocal that = (PerfilesLocal) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (descripcion != null ? !descripcion.equals(that.descripcion) : that.descripcion != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (descripcion != null ? descripcion.hashCode() : 0);
        return result;
    }
}
