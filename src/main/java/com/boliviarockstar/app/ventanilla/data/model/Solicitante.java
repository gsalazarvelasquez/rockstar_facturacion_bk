package com.boliviarockstar.app.ventanilla.data.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by jose on 31/03/2016.
 */
@Entity
public class Solicitante {
    private Long id;
    private String nombres;
    private String apellidos;
    private String ci;
    private String expedicionCi;
    private String telefono;
    private String matricula;

    @Id
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombres", nullable = true, insertable = true, updatable = true, length = 128)
    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    @Basic
    @Column(name = "apellidos", nullable = true, insertable = true, updatable = true, length = 128)
    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    @Basic
    @Column(name = "ci", nullable = true, insertable = true, updatable = true, length = 64)
    public String getCi() {
        return ci;
    }

    public void setCi(String ci) {
        this.ci = ci;
    }

    @Basic
    @Column(name = "expedicion_ci", nullable = true, insertable = true, updatable = true, length = 64)
    public String getExpedicionCi() {
        return expedicionCi;
    }

    public void setExpedicionCi(String expedicionCi) {
        this.expedicionCi = expedicionCi;
    }

    @Basic
    @Column(name = "telefono", nullable = true, insertable = true, updatable = true, length = 128)
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Basic
    @Column(name = "matricula", nullable = true, insertable = true, updatable = true, length = 128)
    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Solicitante that = (Solicitante) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (nombres != null ? !nombres.equals(that.nombres) : that.nombres != null) return false;
        if (apellidos != null ? !apellidos.equals(that.apellidos) : that.apellidos != null) return false;
        if (ci != null ? !ci.equals(that.ci) : that.ci != null) return false;
        if (expedicionCi != null ? !expedicionCi.equals(that.expedicionCi) : that.expedicionCi != null) return false;
        if (telefono != null ? !telefono.equals(that.telefono) : that.telefono != null) return false;
        if (matricula != null ? !matricula.equals(that.matricula) : that.matricula != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (nombres != null ? nombres.hashCode() : 0);
        result = 31 * result + (apellidos != null ? apellidos.hashCode() : 0);
        result = 31 * result + (ci != null ? ci.hashCode() : 0);
        result = 31 * result + (expedicionCi != null ? expedicionCi.hashCode() : 0);
        result = 31 * result + (telefono != null ? telefono.hashCode() : 0);
        result = 31 * result + (matricula != null ? matricula.hashCode() : 0);
        return result;
    }
}
