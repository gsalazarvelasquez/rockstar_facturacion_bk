package com.boliviarockstar.app.ventanilla.data.model;

import javax.persistence.*;

/**
 * Created by daniel on 03/05/2016.
 */
@Entity
@Table(name = "tipo_estadistica_basica", schema = "fundempresa_local", catalog = "")
public class TipoEstadisticaBasica {
    private Long id;
    private String nombre;
    private String disponibilidad;
    private String descripcion;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre", nullable = true, length = 512)
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "disponibilidad", nullable = true, length = 1024)
    public String getDisponibilidad() {
        return disponibilidad;
    }

    public void setDisponibilidad(String disponibilidad) {
        this.disponibilidad = disponibilidad;
    }

    @Basic
    @Column(name = "descripcion", nullable = true, length = 2048)
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TipoEstadisticaBasica that = (TipoEstadisticaBasica) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (nombre != null ? !nombre.equals(that.nombre) : that.nombre != null) return false;
        if (disponibilidad != null ? !disponibilidad.equals(that.disponibilidad) : that.disponibilidad != null)
            return false;
        if (descripcion != null ? !descripcion.equals(that.descripcion) : that.descripcion != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (nombre != null ? nombre.hashCode() : 0);
        result = 31 * result + (disponibilidad != null ? disponibilidad.hashCode() : 0);
        result = 31 * result + (descripcion != null ? descripcion.hashCode() : 0);
        return result;
    }
}
