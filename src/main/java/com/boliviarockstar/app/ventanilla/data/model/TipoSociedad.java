package com.boliviarockstar.app.ventanilla.data.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@Entity
@Table(name = "tipo_sociedad",
        schema = "fundempresa_local", catalog = "",
        uniqueConstraints = {
                @UniqueConstraint(name = "idx_tipos_sociedad", columnNames = {"id"})
        })
public class TipoSociedad {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "valor", length = 255)
    @Size(min = 1, max = 255, message = "Longitud debe estar entre 1 y 255 caracteres")
    private String valor;

//    @Transient
//    @OneToMany(mappedBy = "tipoSocietario", fetch = FetchType.EAGER)
//    private List<Homonimia> homonimiaList;

    public TipoSociedad(Long id, String valor) {
        this.id = id;
        this.valor = valor;
    }

    public TipoSociedad(TipoSociedad tipoSociedad) {
        this.id = tipoSociedad.id;
        this.valor = tipoSociedad.valor;
    }

    public TipoSociedad(Long id) {
        this.id = id;
    }

    public TipoSociedad() {
    }

    @Override
    public String toString() {
        return "TipoSociedad{" +
                "id=" + id +
                ", valor='" + valor +
                '}';
    }

}