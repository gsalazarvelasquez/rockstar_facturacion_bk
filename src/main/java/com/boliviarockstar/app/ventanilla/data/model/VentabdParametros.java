package com.boliviarockstar.app.ventanilla.data.model;

import javax.persistence.*;

/**
 * Created by jose on 28/04/2016.
 */
@Entity
@Table(name = "ventabd_parametros", schema = "", catalog = "fundempresa_local")
public class VentabdParametros {
    private Integer id;
    private String listadoPrecioBase;
    private String listadoPrecioRegistro;
    private String iva;
    private String estbasicaMensuales;
    private String estbasicaDepartamentales;
    private String estbasicaActivEconomica;
    private String estbasicaAnuario;
    private String estbasicaDatosFinancieros;
    private String crucevarCostoFijo;
    private String crucevarCostoVar;
    private String crucevarPrecioRegistro;

    @Id
    @Column(name= "id", nullable= false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "listado_precio_base", nullable = false, insertable = true, updatable = true, length = 20)
    public String getListadoPrecioBase() {
        return listadoPrecioBase;
    }

    public void setListadoPrecioBase(String listadoPrecioBase) {
        this.listadoPrecioBase = listadoPrecioBase;
    }

    @Basic
    @Column(name = "listado_precio_registro", nullable = false, insertable = true, updatable = true, length = 20)
    public String getListadoPrecioRegistro() {
        return listadoPrecioRegistro;
    }

    public void setListadoPrecioRegistro(String listadoPrecioRegistro) {
        this.listadoPrecioRegistro = listadoPrecioRegistro;
    }

    @Basic
    @Column(name = "iva", nullable = false, insertable = true, updatable = true, length = 20)
    public String getIva() {
        return iva;
    }

    public void setIva(String iva) {
        this.iva = iva;
    }

    @Basic
    @Column(name = "estbasica_mensuales", nullable = false, insertable = true, updatable = true, length = 20)
    public String getEstbasicaMensuales() {
        return estbasicaMensuales;
    }

    public void setEstbasicaMensuales(String estbasicaMensuales) {
        this.estbasicaMensuales = estbasicaMensuales;
    }

    @Basic
    @Column(name = "estbasica_departamentales", nullable = false, insertable = true, updatable = true, length = 20)
    public String getEstbasicaDepartamentales() {
        return estbasicaDepartamentales;
    }

    public void setEstbasicaDepartamentales(String estbasicaDepartamentales) {
        this.estbasicaDepartamentales = estbasicaDepartamentales;
    }

    @Basic
    @Column(name = "estbasica_activ_economica", nullable = false, insertable = true, updatable = true, length = 20)
    public String getEstbasicaActivEconomica() {
        return estbasicaActivEconomica;
    }

    public void setEstbasicaActivEconomica(String estbasicaActivEconomica) {
        this.estbasicaActivEconomica = estbasicaActivEconomica;
    }

    @Basic
    @Column(name = "estbasica_anuario", nullable = false, insertable = true, updatable = true, length = 20)
    public String getEstbasicaAnuario() {
        return estbasicaAnuario;
    }

    public void setEstbasicaAnuario(String estbasicaAnuario) {
        this.estbasicaAnuario = estbasicaAnuario;
    }

    @Basic
    @Column(name = "estbasica_datos_financieros", nullable = false, insertable = true, updatable = true, length = 20)
    public String getEstbasicaDatosFinancieros() {
        return estbasicaDatosFinancieros;
    }

    public void setEstbasicaDatosFinancieros(String estbasicaDatosFinancieros) {
        this.estbasicaDatosFinancieros = estbasicaDatosFinancieros;
    }

    @Basic
    @Column(name = "crucevar_costo_fijo", nullable = false, insertable = true, updatable = true, length = 20)
    public String getCrucevarCostoFijo() {
        return crucevarCostoFijo;
    }

    public void setCrucevarCostoFijo(String crucevarCostoFijo) {
        this.crucevarCostoFijo = crucevarCostoFijo;
    }

    @Basic
    @Column(name = "crucevar_costo_var", nullable = false, insertable = true, updatable = true, length = 20)
    public String getCrucevarCostoVar() {
        return crucevarCostoVar;
    }

    public void setCrucevarCostoVar(String crucevarCostoVar) {
        this.crucevarCostoVar = crucevarCostoVar;
    }

    @Basic
    @Column(name = "crucevar_precio_registro", nullable = false, insertable = true, updatable = true, length = 20)
    public String getCrucevarPrecioRegistro() {
        return crucevarPrecioRegistro;
    }

    public void setCrucevarPrecioRegistro(String crucevarPrecioRegistro) {
        this.crucevarPrecioRegistro = crucevarPrecioRegistro;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        VentabdParametros that = (VentabdParametros) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (listadoPrecioBase != null ? !listadoPrecioBase.equals(that.listadoPrecioBase) : that.listadoPrecioBase != null)
            return false;
        if (listadoPrecioRegistro != null ? !listadoPrecioRegistro.equals(that.listadoPrecioRegistro) : that.listadoPrecioRegistro != null)
            return false;
        if (iva != null ? !iva.equals(that.iva) : that.iva != null) return false;
        if (estbasicaMensuales != null ? !estbasicaMensuales.equals(that.estbasicaMensuales) : that.estbasicaMensuales != null)
            return false;
        if (estbasicaDepartamentales != null ? !estbasicaDepartamentales.equals(that.estbasicaDepartamentales) : that.estbasicaDepartamentales != null)
            return false;
        if (estbasicaActivEconomica != null ? !estbasicaActivEconomica.equals(that.estbasicaActivEconomica) : that.estbasicaActivEconomica != null)
            return false;
        if (estbasicaAnuario != null ? !estbasicaAnuario.equals(that.estbasicaAnuario) : that.estbasicaAnuario != null)
            return false;
        if (estbasicaDatosFinancieros != null ? !estbasicaDatosFinancieros.equals(that.estbasicaDatosFinancieros) : that.estbasicaDatosFinancieros != null)
            return false;
        if (crucevarCostoFijo != null ? !crucevarCostoFijo.equals(that.crucevarCostoFijo) : that.crucevarCostoFijo != null)
            return false;
        if (crucevarCostoVar != null ? !crucevarCostoVar.equals(that.crucevarCostoVar) : that.crucevarCostoVar != null)
            return false;
        if (crucevarPrecioRegistro != null ? !crucevarPrecioRegistro.equals(that.crucevarPrecioRegistro) : that.crucevarPrecioRegistro != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (listadoPrecioBase != null ? listadoPrecioBase.hashCode() : 0);
        result = 31 * result + (listadoPrecioRegistro != null ? listadoPrecioRegistro.hashCode() : 0);
        result = 31 * result + (iva != null ? iva.hashCode() : 0);
        result = 31 * result + (estbasicaMensuales != null ? estbasicaMensuales.hashCode() : 0);
        result = 31 * result + (estbasicaDepartamentales != null ? estbasicaDepartamentales.hashCode() : 0);
        result = 31 * result + (estbasicaActivEconomica != null ? estbasicaActivEconomica.hashCode() : 0);
        result = 31 * result + (estbasicaAnuario != null ? estbasicaAnuario.hashCode() : 0);
        result = 31 * result + (estbasicaDatosFinancieros != null ? estbasicaDatosFinancieros.hashCode() : 0);
        result = 31 * result + (crucevarCostoFijo != null ? crucevarCostoFijo.hashCode() : 0);
        result = 31 * result + (crucevarCostoVar != null ? crucevarCostoVar.hashCode() : 0);
        result = 31 * result + (crucevarPrecioRegistro != null ? crucevarPrecioRegistro.hashCode() : 0);
        return result;
    }
}
