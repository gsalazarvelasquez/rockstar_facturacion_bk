package com.boliviarockstar.app.ventanilla.data.repository;

import com.boliviarockstar.shared.persistence.Repository;

import javax.ejb.Stateless;
import javax.persistence.Query;
import java.util.List;

@Stateless
public class CodigosSeguridadRepositoryImpl extends Repository {

    public List<Long> getLast(){

        Query query = em.createQuery("SELECT codigos.codigo FROM CodigosSeguridad codigos ORDER BY codigos.id DESC");
        List<Long> result = query.getResultList();

        return result;
    }

    @Override
    protected Class getPersistentClass() {
        return this.getClass();
    }
}
