package com.boliviarockstar.app.ventanilla.data.repository;

import com.boliviarockstar.app.ventanilla.data.model.Fotocopia;
import com.boliviarockstar.shared.persistence.Repository;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by daniel on 24/02/2016.
 */
public class FotocopiasRepositoryImpl extends Repository {

    @Override
    protected Class getPersistentClass() {
        return this.getClass();
    }

    public List<Fotocopia> getAllOrderByCreationDate(){
        List<Fotocopia> fotocopiaList = this.findAll(Fotocopia.class);
        Collections.sort(fotocopiaList, new Comparator<Fotocopia>() {
            @Override
            public int compare(Fotocopia o1, Fotocopia o2) {
                return o2.getFechaCreacion() != null && o1.getFechaCreacion() != null
                        ? o2.getFechaCreacion().compareTo(o1.getFechaCreacion())
                        : o2.getId().compareTo(o1.getId());
            }
        });

        return fotocopiaList;
    }
}
