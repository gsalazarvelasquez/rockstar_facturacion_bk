package com.boliviarockstar.app.ventanilla.data.repository;

import com.boliviarockstar.app.ventanilla.data.model.TipoSociedad;
import com.boliviarockstar.shared.persistence.Repository;

/**
 * Created by daniel on 11/02/2016.
 */
public class TipoSociedadRepositoryImpl extends Repository {

    @Override
    protected Class getPersistentClass() {
        return TipoSociedad.class;
    }
}
