package com.boliviarockstar.app.ventanilla.data.repository;

import com.boliviarockstar.app.ventanilla.data.model.VentabdParametros;
import com.boliviarockstar.shared.persistence.Repository;

import java.util.Optional;

/**
 * Created by daniel on 11/02/2016.
 */
public class VentaBDRepositoryImpl extends Repository {

    public Optional<VentabdParametros> getConfig() {
        return getById(VentabdParametros.class, 1L);
    }

    @Override
    protected Class getPersistentClass() {
        return this.getClass();
    }
}
