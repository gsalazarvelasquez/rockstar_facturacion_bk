package com.boliviarockstar.app.ventanilla.exception;

import javax.ejb.ApplicationException;

/**
 * Created by jorgeburgos on 2/1/16.
 */
@ApplicationException
public class PerfilExisteException extends Exception {

    public PerfilExisteException(){
        super("Ya existe la asociacion seleccionada.");
    }

    public PerfilExisteException(Throwable throwable){
        super(throwable);
    }
}
