package com.boliviarockstar.app.ventanilla.exception;

import javax.ejb.ApplicationException;

/**
 * Created by jorgeburgos on 2/1/16.
 */
@ApplicationException
public class TipoSociedadExistentException extends RuntimeException {
}
