package com.boliviarockstar.app.ventanilla.exception;

import javax.ejb.ApplicationException;

/**
 * Created by miguel on 1/14/16.
 */
@ApplicationException
public class TipoSociedadNotFoundException extends RuntimeException {
}
