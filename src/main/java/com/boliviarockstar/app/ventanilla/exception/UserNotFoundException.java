package com.boliviarockstar.app.ventanilla.exception;

/**
 * Created by daniel on 17/05/2016.
 */
public class UserNotFoundException extends Exception{

    public UserNotFoundException(){
        super("User not found.");
    }
}
