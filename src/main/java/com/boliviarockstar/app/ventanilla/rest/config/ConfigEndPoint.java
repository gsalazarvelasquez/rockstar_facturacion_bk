package com.boliviarockstar.app.ventanilla.rest.config;

import com.boliviarockstar.app.billing.data.model.Dosificacion;
import com.boliviarockstar.app.billing.data.model.FundempresaInfo;
import com.boliviarockstar.app.ventanilla.data.dto.JsonResult;
import com.boliviarockstar.app.ventanilla.data.dto.PerfilAdabasDto;
import com.boliviarockstar.app.ventanilla.data.model.PerfilesAdabas;
import com.boliviarockstar.app.ventanilla.data.model.PerfilesLocal;
import com.boliviarockstar.app.ventanilla.data.model.VentabdParametros;
import com.boliviarockstar.app.ventanilla.services.ConfigService;
import com.boliviarockstar.app.ventanilla.services.VentaBDParametrosService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/config")
@Stateless
@Produces(MediaType.APPLICATION_JSON)
public class ConfigEndPoint {

    @Inject
    public VentaBDParametrosService ventaBDParametrosService;

    @Inject
    public ConfigService configService;

    @POST
    @Path("/getFundempresaInfo")
    public JsonResult getFundempresaInfo() {
        JsonResult result = new JsonResult();
        try {
            FundempresaInfo info = configService.fundempresaInfo();
            result.setSuccess(true);
            result.setData(info);
            result.setMessage("Procesado correctamente");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(null);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }


    @POST
    @Path("/updateFundempresaInfo")
    public JsonResult updateFundempresaInfo(FundempresaInfo info) {
        JsonResult result = new JsonResult();
        try {
            boolean response = configService.updateFundempresaInfo(info);
            result.setSuccess(true);
            result.setData(response);
            result.setMessage("Procesado correctamente");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(null);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    @POST
    @Path("/getDosificaciones")
    public JsonResult getDosificaciones() {
        JsonResult result = new JsonResult();
        try {
            List<Dosificacion> config = configService.dosificaciones();
            result.setSuccess(true);
            result.setData(config);
            result.setMessage("Procesado correctamente");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(null);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    @POST
    @Path("/addDosificacion")
    public JsonResult addDosificacion(Dosificacion dosificacion) {
        JsonResult result = new JsonResult();
        try {
            boolean response = configService.addDosificacion(dosificacion);
            result.setSuccess(true);
            result.setData(response);
            result.setMessage("Procesado correctamente");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(null);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    @POST
    @Path("/enableDosificacion")
    public JsonResult enableDosificacion(long idDosificacion) {
        JsonResult result = new JsonResult();
        try {
            boolean response = configService.enableDosificacion(idDosificacion);
            result.setSuccess(true);
            result.setData(response);
            result.setMessage("Procesado correctamente");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(null);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    @POST
    @Path("/disableDosificacion")
    public JsonResult disableDosificacion(long idDosificacion) {
        JsonResult result = new JsonResult();
        try {
            boolean response = configService.disableDosificacion(idDosificacion);
            result.setSuccess(true);
            result.setData(response);
            result.setMessage("Procesado correctamente");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(null);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    @POST
    @Path("/getPrecios")
    public JsonResult getPrecios() {
        JsonResult result = new JsonResult();
        try {
            VentabdParametros config = ventaBDParametrosService.getConfig().get();
            result.setSuccess(true);
            result.setData(config);
            result.setMessage("Procesado correctamente");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(null);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    @POST
    @Path("/updatePrices")
    public JsonResult updatePrices(VentabdParametros requestDto) {
        JsonResult result = new JsonResult();
        try {
            boolean response = ventaBDParametrosService.update(requestDto);
            result.setSuccess(true);
            result.setData(response);
            result.setMessage("Procesado correctamente");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(false);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    @POST
    @Path("/getPerfilesLocales")
    public JsonResult getPerfilesLocales() {
        JsonResult result = new JsonResult();
        try {
            List<PerfilesLocal> perfiles = configService.perfiles();
            result.setSuccess(true);
            result.setData(perfiles);
            result.setMessage("Procesado correctamente");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(null);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    @POST
    @Path("/getAsociacionesPerfil")
    public JsonResult getAsociacionesPerfil() {
        JsonResult result = new JsonResult();
        try {
            List<PerfilesAdabas> perfiles = configService.asociaciones();
            result.setSuccess(true);
            result.setData(perfiles);
            result.setMessage("Procesado correctamente");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(null);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    @POST
    @Path("/deletePerfil")
    public JsonResult deletePerfil(int idAsociacion) {
        JsonResult result = new JsonResult();
        try {
            boolean response = configService.deletePerfilAdabas(idAsociacion);
            result.setSuccess(true);
            result.setData(response);
            result.setMessage("Procesado correctamente");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(false);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    @POST
    @Path("/savePerfil")
    public JsonResult savePerfil(PerfilesAdabas perfil) {
        JsonResult result = new JsonResult();
        try {
            boolean response = configService.save(perfil);
            result.setSuccess(response);
            result.setData(response);
            result.setMessage(response ? "Procesado correctamente" : "Error: Revise los parametros seleccionados.");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(false);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }
}