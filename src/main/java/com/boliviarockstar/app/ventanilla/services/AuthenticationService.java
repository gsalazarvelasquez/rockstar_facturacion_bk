package com.boliviarockstar.app.ventanilla.services;

import com.boliviarockstar.app.ventanilla.exception.UserNotFoundException;
import com.boliviarockstar.shared.security.dto.AccountDto;
import com.boliviarockstar.shared.security.dto.UsernamePasswordCredential;

/**
 * Created by daniel on 17/05/2016.
 */
public interface AuthenticationService {

    AccountDto authenticate(UsernamePasswordCredential usernamePasswordCredential) throws UserNotFoundException;
}
