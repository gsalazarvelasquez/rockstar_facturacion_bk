package com.boliviarockstar.app.ventanilla.services;

import java.util.List;

/**
 * Created by daniel on 24/02/2016.
 */
public interface BasicService {

    <T> List<T> getAll();

    <T> T getById();

    <T> void save(T entity);

    <T> void delete(T entity);
}
