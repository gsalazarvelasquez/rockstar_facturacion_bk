package com.boliviarockstar.app.ventanilla.services;

import com.boliviarockstar.app.billing.data.model.Dosificacion;
import com.boliviarockstar.app.billing.data.model.FundempresaInfo;
import com.boliviarockstar.app.ventanilla.data.model.PerfilesAdabas;
import com.boliviarockstar.app.ventanilla.data.model.PerfilesLocal;
import com.boliviarockstar.app.ventanilla.exception.PerfilExisteException;

import java.util.List;

/**
 * Created by daniel on 24/02/2016.
 */
public interface ConfigService {

    FundempresaInfo fundempresaInfo();

    Boolean updateFundempresaInfo(FundempresaInfo info) throws Exception;

    List<Dosificacion> dosificaciones();

    Boolean addDosificacion(Dosificacion dosage) throws Exception;

    Boolean enableDosificacion(long id) throws Exception;

    Boolean disableDosificacion(long id) throws Exception;

    List<PerfilesLocal> perfiles();

    List<PerfilesAdabas> asociaciones();

    Boolean update(PerfilesAdabas perfil);

    Boolean deletePerfilAdabas(int id) throws Exception;

    Boolean save(PerfilesAdabas perfil) throws PerfilExisteException;

}
