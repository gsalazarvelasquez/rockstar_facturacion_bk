package com.boliviarockstar.app.ventanilla.services;

import com.boliviarockstar.app.ventanilla.data.model.Solicitante;

/**
 * Created by daniel on 16/02/2016.
 */
public interface SolicitanteService {

    Solicitante getSolicitanteById(Long id);
}
