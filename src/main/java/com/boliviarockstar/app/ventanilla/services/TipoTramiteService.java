package com.boliviarockstar.app.ventanilla.services;

import com.boliviarockstar.app.ventanilla.data.model.TipoTramite;

import java.util.List;

/**
 * Created by daniel on 24/02/2016.
 */
public interface TipoTramiteService {

    List<TipoTramite> getAll();
}
