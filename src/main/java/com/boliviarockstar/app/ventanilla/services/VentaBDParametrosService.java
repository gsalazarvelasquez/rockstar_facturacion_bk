package com.boliviarockstar.app.ventanilla.services;

import com.boliviarockstar.app.ventanilla.data.model.VentabdParametros;

import javax.ejb.Local;
import java.util.Optional;

/**
 * Created by miguel on 1/26/16.
 */
@Local
public interface VentaBDParametrosService {

      Optional<VentabdParametros> getConfig();

      Boolean update(VentabdParametros ventaBDParametros);
}
