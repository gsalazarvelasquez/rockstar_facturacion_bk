package com.boliviarockstar.app.ventanilla.services.internal;

import com.boliviarockstar.app.billing.data.model.Dosificacion;
import com.boliviarockstar.app.billing.data.model.FundempresaInfo;
import com.boliviarockstar.app.billing.data.repository.DosificacionRepositoryImpl;
import com.boliviarockstar.app.billing.data.repository.FundempresaInfoRepositoryImpl;
import com.boliviarockstar.app.ventanilla.data.model.PerfilesAdabas;
import com.boliviarockstar.app.ventanilla.data.model.PerfilesLocal;
import com.boliviarockstar.app.ventanilla.data.repository.ConfigRepositoryImpl;
import com.boliviarockstar.app.ventanilla.exception.PerfilExisteException;
import com.boliviarockstar.app.ventanilla.services.ConfigService;

import javax.inject.Inject;
import java.util.List;

public class ConfigServiceImpl implements ConfigService {

    @Inject
    private ConfigRepositoryImpl configRepositoryImpl;

    @Inject
    private DosificacionRepositoryImpl dosificacionRepositoryImpl;

    @Inject
    private FundempresaInfoRepositoryImpl fundempresaInfoRepositoryImpl;

    @Override
    public FundempresaInfo fundempresaInfo() {
        return fundempresaInfoRepositoryImpl.getInfo();
    }

    @Override
    public Boolean updateFundempresaInfo(FundempresaInfo info) throws Exception {
        fundempresaInfoRepositoryImpl.update(info);
        return true;
    }

    @Override
    public List<Dosificacion> dosificaciones() {
        return dosificacionRepositoryImpl.getDosages();
    }

    @Override
    public Boolean disableDosificacion(long id) throws Exception{
        Dosificacion obj = dosificacionRepositoryImpl.getDosage(id);
        if(obj != null){
            obj.setEstado(0L);
            dosificacionRepositoryImpl.update(obj);
        }
        return (obj != null);
    }

    @Override
    public Boolean enableDosificacion(long id) throws Exception{
        Dosificacion obj = dosificacionRepositoryImpl.getDosage(id);
        if(obj != null){
            obj.setEstado(1L);
            dosificacionRepositoryImpl.update(obj);
        }
        return (obj != null);
    }

    @Override
    public Boolean addDosificacion(Dosificacion dosage) throws Exception {
        dosificacionRepositoryImpl.save(dosage);
        return true;
    }

    @Override
    public List<PerfilesLocal> perfiles() {
        return configRepositoryImpl.getPerfiles();
    }

    @Override
    public List<PerfilesAdabas> asociaciones() {
        return configRepositoryImpl.getAsociaciones();
    }

    @Override
    public Boolean update(PerfilesAdabas perfil) {
        configRepositoryImpl.update(perfil);
        return true;
    }

    @Override
    public Boolean deletePerfilAdabas(int id) throws Exception{
        PerfilesAdabas obj = configRepositoryImpl.getAsociacion(id);
        if(obj != null){
            configRepositoryImpl.deleteAndFlush(obj);
        }
        return (obj != null);
    }

    @Override
    public Boolean save(PerfilesAdabas perfil) throws PerfilExisteException {
        if(perfil == null || perfil.getPerfilAdabas() == null || perfil.getPerfilLocal() == null)
            return false;
        PerfilesAdabas exist = configRepositoryImpl.getPerfil(perfil.getPerfilAdabas(), perfil.getPerfilLocal());
        if(exist != null)
            throw new PerfilExisteException();
        configRepositoryImpl.save(perfil);
        return true;
    }
}
