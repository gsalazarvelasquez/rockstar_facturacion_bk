package com.boliviarockstar.app.ventanilla.services.internal;

import com.boliviarockstar.app.ventanilla.data.model.TipoTramite;
import com.boliviarockstar.app.ventanilla.data.repository.TipoTramiteRepositoryImpl;
import com.boliviarockstar.app.ventanilla.services.TipoTramiteService;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by daniel on 24/02/2016.
 */
public class TipoTramiteServiceImpl implements TipoTramiteService {

    @Inject
    private TipoTramiteRepositoryImpl tipoTramiteRepository;

    @Override
    public List<TipoTramite> getAll() {
        List<TipoTramite> result = this.tipoTramiteRepository.findAll(TipoTramite.class);
        return result;
    }
}
