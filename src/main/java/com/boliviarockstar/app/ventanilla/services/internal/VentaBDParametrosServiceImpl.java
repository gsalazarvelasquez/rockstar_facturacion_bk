package com.boliviarockstar.app.ventanilla.services.internal;


import com.boliviarockstar.app.ventanilla.data.model.VentabdParametros;
import com.boliviarockstar.app.ventanilla.data.repository.VentaBDRepositoryImpl;
import com.boliviarockstar.app.ventanilla.services.VentaBDParametrosService;

import javax.inject.Inject;
import java.util.Optional;

/**
 * Created by jorgeburgos on 1/30/16.
 */
//@Stateless
public class VentaBDParametrosServiceImpl implements VentaBDParametrosService {

    @Inject
    VentaBDRepositoryImpl ventaBDRepositoryImpl;

    @Override
    public Optional<VentabdParametros> getConfig() {
        return this.ventaBDRepositoryImpl.getConfig();
    }

    @Override
    public Boolean update(VentabdParametros ventaBDParametros){
        boolean result = true;
        try {
            ventaBDRepositoryImpl.update(ventaBDParametros);
        }catch (Exception e){
            result = false;
            e.printStackTrace();
        }

        return result;
    }
}
