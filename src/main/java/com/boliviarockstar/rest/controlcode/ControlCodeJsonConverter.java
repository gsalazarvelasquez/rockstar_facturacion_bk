package com.boliviarockstar.rest.controlcode;

import com.boliviarockstar.app.impuestos.data.dto.ControlEntityDto;
import com.boliviarockstar.shared.json.EntityJsonConverter;
import com.boliviarockstar.shared.json.JsonReader;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class ControlCodeJsonConverter implements EntityJsonConverter<ControlEntityDto> {


    @Override
    public ControlEntityDto convertFrom(String json) {
        final JsonObject jsonObject = JsonReader.readAsJsonObject(json);

        final ControlEntityDto application = new ControlEntityDto();
        application.setAuthorizationStr(JsonReader.getStringOrNull(jsonObject, "authorizationStr"));
        application.setInvoiceNumberStr(JsonReader.getStringOrNull(jsonObject, "invoiceNumberStr"));
        application.setClientNit(JsonReader.getStringOrNull(jsonObject, "clientNit"));
        application.setDateStr(JsonReader.getStringOrNull(jsonObject, "dateStr"));
        application.setAmountStr(JsonReader.getStringOrNull(jsonObject, "amountStr"));
        application.setDosageKey(JsonReader.getStringOrNull(jsonObject, "dosageKey"));
        return application;
    }

    @Override
    public JsonElement convertToJsonElement(ControlEntityDto application) {
        JsonObject jsonObject = new JsonObject();
        return jsonObject;
    }
}
