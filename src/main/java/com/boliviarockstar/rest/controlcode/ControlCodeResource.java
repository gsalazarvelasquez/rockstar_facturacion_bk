package com.boliviarockstar.rest.controlcode;

import com.boliviarockstar.app.impuestos.data.dto.ControlEntityDto;
import com.boliviarockstar.app.impuestos.services.ControlGenerationService;
import com.boliviarockstar.shared.exception.FieldNotValidException;
import com.boliviarockstar.shared.json.OperationResultJsonWriter;
import com.boliviarockstar.shared.rest.HttpCode;
import com.boliviarockstar.shared.rest.OperationResult;
import com.boliviarockstar.shared.rest.ResourceMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.ParseException;

import static com.boliviarockstar.shared.rest.StandardOperationResults.getOperationResultInvalidField;

@Path("/codigo_de_control")
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces({MediaType.APPLICATION_JSON})
public class ControlCodeResource {

    private Logger logger = LoggerFactory.getLogger(getClass());
    private static final ResourceMessage RESOURCE_MESSAGE = new ResourceMessage("codigo_de_control");

    @Inject
    public ControlCodeJsonConverter controlCodeJsonConverter;

    @Inject
    public ControlGenerationService controlGenerationService;

    /**
     * add an application
     *
     * @param body
     * @return
     */
    @POST
    @Path("/generate")
    public Response addApplication(String body) {
        logger.debug("Adding a new application with body {}", body);

        ControlEntityDto controlEntityDto = controlCodeJsonConverter.convertFrom(body);
//        application.setId(null);

        HttpCode httpCode = HttpCode.CREATED;
        OperationResult result = null;

        try {
            String controlCode = controlGenerationService.generate(controlEntityDto);
            result = OperationResult.success(controlCode);
        } catch (FieldNotValidException e) {
            logger.error("One of the fields of application is not valid", e);
            httpCode = HttpCode.VALIDATION_ERROR;
            result = getOperationResultInvalidField(RESOURCE_MESSAGE, e);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        logger.debug("Returning the operation result after adding role: {}", result);
        return Response.status(httpCode.getCode()).entity(OperationResultJsonWriter.toJson(result)).build();
    }

    @GET
    @Path("/salute")
    @Produces({MediaType.APPLICATION_JSON})
    public Response salute() {
        return Response.status(HttpCode.OK.getCode()).entity("Hello!").build();
    }
}