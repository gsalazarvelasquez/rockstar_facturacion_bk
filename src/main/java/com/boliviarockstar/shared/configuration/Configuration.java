package com.boliviarockstar.shared.configuration;

import org.apache.commons.lang.StringUtils;

import java.util.Properties;

public class Configuration {

	public static Properties props = new Properties();

    public static String getStringS(final ConfigurationKey key) {
        return props.getProperty(key.getKey());
    }

    public static String getS(final ConfigurationKey key) {
        return props.getProperty(key.getKey());
    }

    public static Integer getIntS(final ConfigurationKey key) {
        return Integer.parseInt(getStringS(key));
    }

    public static boolean getBooleanS(final ConfigurationKey key) {
        return Boolean.valueOf(getStringS(key));
    }

    public static String[] getArrayS(final ConfigurationKey key) {
        final String value = getStringS(key);
        return (value == null ? new String[] {} : value.split(","));
    }

    public static String getStringSValid(final ConfigurationKey key) {
        String value = getStringS(key);
        if (StringUtils.isEmpty(value)) {
            throw new IllegalArgumentException("Parameter invalid [ " + key.getKey() + " ] ");
        }
        return value;
    }
    public int size() {
        return props.size();
    }

    public String getProperty(final String key) {
        return props.getProperty(key);
    }
}
