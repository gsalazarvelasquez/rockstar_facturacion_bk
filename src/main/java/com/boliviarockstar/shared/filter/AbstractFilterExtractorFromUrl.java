package com.boliviarockstar.shared.filter;

import javax.ws.rs.core.UriInfo;

/**
 * Created by jorgeburgos on 1/18/16.
 */
public abstract class AbstractFilterExtractorFromUrl {
    private UriInfo uriInfo;
    private static final int DEFAULT_PAGE = 0;
    private static final int DEFAULT_PER_PAGE = 10;

    protected abstract String getDefaultSortField();

    public AbstractFilterExtractorFromUrl(UriInfo uriInfo) {
        this.uriInfo = uriInfo;
    }

    protected UriInfo getUriInfo() {
        return uriInfo;
    }

    protected PaginationData extractPaginationData() {
        int perPage = getPerPage();
        int firstResult = getPage() * perPage;

        String orderField;
        PaginationData.OrderMode orderMode;
        String sortField = getSortField();

        if (sortField.startsWith("+")) {
            orderField = sortField.substring(1);
            orderMode = PaginationData.OrderMode.ASCENDING;
        } else if (sortField.startsWith("-")) {
            orderField = sortField.substring(1);
            orderMode = PaginationData.OrderMode.DESCENDING;
        } else {
            orderField = sortField;
            orderMode = PaginationData.OrderMode.ASCENDING;
        }

        return new PaginationData(firstResult, perPage, orderField, orderMode);
    }

    private String getSortField() {
        String sortField = uriInfo.getQueryParameters().getFirst("sort");
        if (sortField == null) {
            return getDefaultSortField();
        }

        return sortField;
    }

    private Integer getPage() {
        String page = uriInfo.getQueryParameters().getFirst("page");
        if (page == null) {
            return DEFAULT_PAGE;
        }
        return Integer.parseInt(page);
    }

    private Integer getPerPage() {
        String perPage = uriInfo.getQueryParameters().getFirst("per_page");
        if (perPage == null) {
            return DEFAULT_PER_PAGE;
        }

        return Integer.parseInt(perPage);
    }
}
