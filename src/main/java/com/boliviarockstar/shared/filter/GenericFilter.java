package com.boliviarockstar.shared.filter;

/**
 * Created by jorgeburgos on 1/28/16.
 */
public class GenericFilter {
    private PaginationData paginationData;

    public GenericFilter() {
    }

    public GenericFilter(PaginationData paginationData) {
        this.paginationData = paginationData;
    }

    public PaginationData getPaginationData() {
        return paginationData;
    }

    public void setPaginationData(PaginationData paginationData) {
        this.paginationData = paginationData;
    }

    public boolean hasPaginationData() {
        return getPaginationData() != null;
    }

    public boolean hasOrderField() {
        return hasPaginationData() && getPaginationData().getOrderField() != null;
    }


}
