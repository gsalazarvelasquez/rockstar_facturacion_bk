package com.boliviarockstar.shared.json;

import com.boliviarockstar.shared.json.exception.InvalidJsonException;
import com.google.gson.*;

/**
 * Created by jorgeburgos on 12/23/15.
 */
public class JsonReader {
    public static JsonObject readAsJsonObject(String json) throws InvalidJsonException {
        return readJsonAs(json, JsonObject.class);
    }

    public static JsonArray readAsJsonArray(String json)  throws InvalidJsonException {
        return readJsonAs(json, JsonArray.class);
    }

    public static <T> T readJsonAs(final String json, final Class<T> jsonClass)  throws InvalidJsonException {
        if (json == null || json.trim().isEmpty()) {
            throw new InvalidJsonException("Json String can not be null");
        }

        try {
            return new Gson().fromJson(json, jsonClass);
        } catch (JsonSyntaxException e) {
            throw new InvalidJsonException(e);
        }
    }

    public static Long getLongOrNull(final JsonObject jsonObject, final String propertyName) {
        final JsonElement property = jsonObject.get(propertyName);
        if (isJsonElementNull(property)) {
            return null;
        }

        return property.getAsLong();
    }

    public static Integer getIntegerOrNull(final JsonObject jsonObject, final String propertyName) {
        final JsonElement property = jsonObject.get(propertyName);
        if (isJsonElementNull(property)) {
            return null;
        }

        return property.getAsInt();
    }

    public static String getStringOrNull(final JsonObject jsonObject, final String propertyName) {
        final JsonElement property = jsonObject.get(propertyName);
        if (isJsonElementNull(property)) {
            return null;
        }

        return property.getAsString();
    }

    public static Double getDoubleOrNull(final JsonObject jsonObject, final String propertyName) {
        final JsonElement property = jsonObject.get(propertyName);
        if (isJsonElementNull(property)) {
            return null;
        }

        return property.getAsDouble();
    }

    private static boolean isJsonElementNull(final JsonElement element) {
        return element == null || element.isJsonNull();
    }


}
