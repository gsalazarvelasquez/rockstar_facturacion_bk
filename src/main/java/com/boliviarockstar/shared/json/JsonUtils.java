package com.boliviarockstar.shared.json;

import com.boliviarockstar.shared.filter.PaginatedData;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * Created by jorgeburgos on 12/23/15.
 */
public final class JsonUtils {
    public JsonUtils() {
    }

    public static JsonElement getJsonElementWithId(Long id) {
        JsonObject idJson = new JsonObject();
        idJson.addProperty("id", id);

        return idJson;
    }

    public static <T> JsonElement getJsonElementWithPagingAndEntries(PaginatedData<T> paginatedData,
                                                                     EntityJsonConverter<T> entityJsonConverter) {
        JsonObject jsonWithEntriesAndPaging = new JsonObject();

        JsonObject jsonPaging = new JsonObject();
        jsonPaging.addProperty("totalRecords", paginatedData.getNumberOfRows());

        jsonWithEntriesAndPaging.add("paging", jsonPaging);
        jsonWithEntriesAndPaging.add("entries", entityJsonConverter.convertToJsonElement(paginatedData.getRows()));

        return jsonWithEntriesAndPaging;
    }
}
