package com.boliviarockstar.shared.json;

import com.google.gson.Gson;

/**
 * Created by jorgeburgos on 12/24/15.
 */
public final class JsonWriter {
    private JsonWriter() {
    }

    public static String writeToString(Object object) {
        if (object == null) {
            return "";
        }

        return new Gson().toJson(object);
    }
}
