package com.boliviarockstar.shared.rest;

import com.boliviarockstar.shared.exception.FieldNotValidException;

/**
 * Created by jorgeburgos on 12/24/15.
 */
public class StandardOperationResults {
    public StandardOperationResults() {
    }

    public static OperationResult getOperationResultExistent(ResourceMessage resourceMessage, String fieldNames) {
        return OperationResult.error(resourceMessage.getKeyOfResourceExistent(),
                resourceMessage.getMessageOfResourceExistent(fieldNames));
    }

    public static OperationResult getOperationResultInvalidField(ResourceMessage resourceMessage, FieldNotValidException ex) {
        return OperationResult.error(resourceMessage.getKeyOfInvalidField(ex.getFieldName()),
                ex.getMessage());
    }

    public static OperationResult getOperationResultNotFound(ResourceMessage resourceMessage) {
        return OperationResult.error(resourceMessage.getKeyOfResourceNotFound(),
                resourceMessage.getMessageOfResourceNotFound());
    }

    public static OperationResult getOperationResultDependencyNotFound(ResourceMessage resourceMessage, String dependencyField) {
        return OperationResult.error(resourceMessage.getKeyOfInvalidField(dependencyField),
                resourceMessage.getMessageNotFound());
    }


}
