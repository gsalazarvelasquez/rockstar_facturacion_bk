package com.boliviarockstar.shared.utils;

public class LongUtil {

    private LongUtil() {
    }

    public static boolean isEmpty(Long value) {
        return value == null || value == 0;
    }

    public static boolean isEmpty(Integer value) {
        return value == null || value == 0;
    }

    public static boolean isNotEmpty(Long value) {
        return !isEmpty(value);
    }
}